#include "Assignment.hpp"

#include "model.hpp"
#include "UI.hpp"
#include "Scene.hpp"
#include "PNGMaker.hpp"

#define XRES 50
#define YRES 50

using namespace std;
using namespace Eigen;

namespace Assignment {
    /* Assignment Part A, Part 1 */
    static const float k_min_io_test_x = -7.0;
    static const float k_max_io_test_x = 7.0;
    static const float k_min_io_test_y = -7.0;
    static const float k_max_io_test_y = 7.0;
    static const float k_min_io_test_z = -7.0;
    static const float k_max_io_test_z = 7.0;
    static const int k_io_test_x_count = 15;
    static const int k_io_test_y_count = 15;
    static const int k_io_test_z_count = 15;

    struct Ray;
    struct Component;

    float intersectionTest(Renderable *ren, vector<Transformation> &transformation_stack, Ray &camera_ray, vector<Ray> &rays);
    vector<float> get_initial_guess(Renderable *ren, Vector3f &a, Eigen::Vector3f &b);
    float Newtons_method(Primitive *prm, Vector3f &a, Vector3f &b, float initial_guess);
    float sq_io(float e, float n, Vector3f &point);
    Vector3f grad_sq_io(float e, float n, Vector3f &point);
    Matrix4f get_rotation_matrix(Vector3f &axis, float theta);
    Matrix4f get_inv_transform_matrix(vector<Transformation> &transformation_stack);
    Matrix4f get_transform_matrix(vector<Transformation> &transformation_stack);
    Matrix4f get_inv_normal_transform_matrix(vector<Transformation> &transformation_stack);
    Matrix4f get_normal_transform_matrix(vector<Transformation> &transformation_stack);
    Vector3f transform_point(Matrix4f &transmatrix, Vector3f &point);
    void printRay(Ray &ray);

    bool IOTest(
        Renderable *ren,
        vector<Transformation> &transformation_stack,
        float x,
        float y,
        float z)
    {
        if (ren->getType() == PRM) {
            Primitive *prm = dynamic_cast<Primitive*>(ren);

            /* TODO
             *
             *     Apply inverse of the transformations passed in through
             * transformation_stack to (x, y, z) and perform the IO test
             * oulined in the lecture notes. If the point is inside the
             * primitive, return true. Otherwise return false
             **/

            // transform point x, y, z
            Vector3f prm_scale = prm -> getCoeff();
            Vector4f scale;
            scale << prm_scale, 1;
            transformation_stack.push_back(Transformation(SCALE, scale));

            Matrix4f transmatrix = get_inv_transform_matrix(transformation_stack);
            Vector3f point(x, y, z);
            point = transform_point(transmatrix, point);
            bool isinside = sq_io(prm->getExp0(), prm->getExp1(), point) <= 0;

            transformation_stack.pop_back();

            return isinside; // <-- replace with your code ***

        } else if (ren->getType() == OBJ) {
            Object *obj = dynamic_cast<Object*>(ren);
            const vector<Transformation>& overall_trans =
            obj->getOverallTransformation();
            for (int i = overall_trans.size() - 1; i >= 0; i--) {
                transformation_stack.push_back(overall_trans.at(i));
            }

            bool IO_result = false;
            for (auto& child_it : obj->getChildren()) {
                const vector<Transformation>& child_trans = 
                child_it.second.transformations;
                for (int i = child_trans.size() - 1; i >= 0; i--) {
                    transformation_stack.push_back(child_trans.at(i));
                }
                IO_result |= IOTest(
                    Renderable::get(child_it.second.name),
                    transformation_stack,
                    x, y, z);
                transformation_stack.erase(
                    transformation_stack.end() - child_trans.size(),
                    transformation_stack.end());
            }

            transformation_stack.erase(
                transformation_stack.end() - overall_trans.size(),
                transformation_stack.end());
            return IO_result;
        } else {
            fprintf(stderr, "Renderer::draw ERROR invalid RenderableType %d\n",
                ren->getType());
            exit(1);
        }

        return false;
    }

    void drawIOTest() {
        const Line* cur_state = CommandLine::getState();
        Renderable* current_selected_ren = NULL;

        if (cur_state) {
            current_selected_ren = Renderable::get(cur_state->tokens[1]);
        }

        if (current_selected_ren == NULL) {
            return;
        }

        const float IO_test_color[3] = {0.5, 0.0, 0.0};
        glMaterialfv(GL_FRONT, GL_AMBIENT, IO_test_color);
        for (int x = 0; x < k_io_test_x_count; x++) {
            for (int y = 0; y < k_io_test_y_count; y++) {
                for (int z = 0; z < k_io_test_z_count; z++) {
                    float test_x = k_min_io_test_x
                    + x * (k_max_io_test_x - k_min_io_test_x)
                    / (float) k_io_test_x_count;
                    float test_y = k_min_io_test_y
                    + y * (k_max_io_test_y - k_min_io_test_y)
                    / (float) k_io_test_y_count;
                    float test_z = k_min_io_test_z
                    + z * (k_max_io_test_z - k_min_io_test_z)
                    / (float) k_io_test_z_count;

                    vector<Transformation> transformation_stack;
                    if (IOTest(
                        current_selected_ren,
                        transformation_stack,
                        test_x,
                        test_y,
                        test_z))
                    {
                        glPushMatrix();
                        glTranslatef(test_x, test_y, test_z);
                        glutWireSphere(0.05, 4, 4);
                        glPopMatrix();
                    }
                }
            }
        }
    }

    /* Assignment Part A, Part 2 */
    struct Ray {
        float origin_x;
        float origin_y;
        float origin_z;

        float direction_x;
        float direction_y;
        float direction_z;

        bool isempty;
        Primitive *prm;
        float dist;

        Vector3f getLocation(float t) {
            Vector3f loc;
            loc << origin_x + t * direction_x,
            origin_y + t * direction_y,
            origin_z + t * direction_z;
            return loc;
        }
    };


    Ray findIntersection(Ray &camera_ray) {
        /* TODO
         *
         **/
        const Line* cur_state = CommandLine::getState();
        Renderable* current_selected_ren = NULL;
        Ray intersection_ray;

        if (cur_state) {
            current_selected_ren = Renderable::get(cur_state->tokens[1]);
        }

        if (current_selected_ren == NULL) {
            return intersection_ray;
        }

        vector<Transformation> transformation_stack;
        vector<Ray> rays;
        Ray ray;

        ray.isempty = true;
        ray.dist = FLT_MAX;

        rays.push_back(ray);
        rays.push_back(ray);
        intersectionTest(current_selected_ren, transformation_stack, camera_ray, rays);
        return rays[0];
        
    }

    void drawIntersectTest(Camera *camera) {
        Ray camera_ray;

        // setup camera location vector
        Vector3f camerapos = camera -> getPosition();
        camera_ray.origin_x = camerapos(0);      // TODO: replace 0.0 with correct value
        camera_ray.origin_y = camerapos(1);      // TODO: replace 0.0 with correct value
        camera_ray.origin_z = camerapos(2);      // TODO: replace 0.0 with correct value

        // setup camera direction vector
        Vector3f axis = camera -> getAxis();
        float angle = camera -> getAngle();

        Matrix4f rotation = get_rotation_matrix(axis, angle);

        Vector3f camera_initial_dir;
        camera_initial_dir << 0, 0, -1;

        Vector3f camera_final_dir = transform_point(rotation, camera_initial_dir);

        camera_ray.direction_x = camera_final_dir(0);   // TODO: replace 0.0 with correct value
        camera_ray.direction_y = camera_final_dir(1);   // TODO: replace 0.0 with correct value
        camera_ray.direction_z = camera_final_dir(2);   // TODO: replace 0.0 with correct value

        Ray intersection_ray = findIntersection(camera_ray);

        const float IO_test_color[3] = {0.0, 0.0, 0.5};
        glMaterialfv(GL_FRONT, GL_AMBIENT, IO_test_color);
        glLineWidth(3.0);
        glBegin(GL_LINES);
        glVertex3f(
            intersection_ray.origin_x,
            intersection_ray.origin_y,
            intersection_ray.origin_z);
        Vector3f endpoint = intersection_ray.getLocation(1.0);
        glVertex3f(endpoint[0], endpoint[1], endpoint[2]);
        glEnd();

    }

    /*void drawIntersectTest(Camera *camera) {

        // setup frustum near value
        float near  = camera -> getNear();

        // setup camera direction vector
        Vector3f axis = camera -> getAxis();
        float angle = camera -> getAngle();

        Matrix4f rotation = get_rotation_matrix(axis, angle);


        Vector3f e1(0, 0, -1);
        e1 = transform_point(rotation, e1);
        Vector3f e2(1, 0, 0);
        e2 = transform_point(rotation, e2);
        Vector3f e3(0, 1, 0);
        e3 = transform_point(rotation, e3);

        Ray camera_ray;
        // setup camera location vector
        Vector3f e = camera -> getPosition();
        camera_ray.origin_x = e(0);      
        camera_ray.origin_y = e(1);     
        camera_ray.origin_z = e(2);

        float h = 2 * near * tan(camera -> getFov() * M_PI / 180 / 2);
        float w = camera -> getAspect() * h;

        for (int x = 0; x < XRES; x++) {
            for (int y = 0; y < YRES; y++) {

                ////////cout << to_string(x) + " " + to_string(y) << endl;
                float xi = (x - 1.0 * XRES / 2) * (w / XRES);
                float yi = (y - 1.0 * YRES / 2) * (h / YRES);

                Vector3f a = near * e1 + xi * e2 + yi * e3;

                camera_ray.direction_x = a(0);
                camera_ray.direction_y = a(1);
                camera_ray.direction_z = a(2);

                Ray point = findIntersection(camera_ray);
            
                const float IO_test_color[3] = {0.0, 0.0, 0.5};
                glMaterialfv(GL_FRONT, GL_AMBIENT, IO_test_color);
                glLineWidth(3.0);
                glBegin(GL_LINES);
                glVertex3f(
                    point.origin_x,
                    point.origin_y,
                    point.origin_z);
                Vector3f endpoint = point.getLocation(1.0);
                glVertex3f(endpoint[0], endpoint[1], endpoint[2]);
                glEnd();
            }
        }

    }*/

    /* Assignment Part B */

    /* Ray traces the scene. */
    void raytrace(Camera camera, Scene scene) {
        // LEAVE THIS UNLESS YOU WANT TO WRITE YOUR OWN OUTPUT FUNCTION
        PNGMaker png = PNGMaker(XRES, YRES);

        // TODO
        // setup frustum near value
        float near  = camera.getNear();

        // setup camera direction vector
        Vector3f axis = camera.getAxis();
        float angle = camera.getAngle();

        Matrix4f rotation = get_rotation_matrix(axis, angle);

        Vector3f e1(0, 0, -1);
        e1 = transform_point(rotation, e1);
        Vector3f e2(1, 0, 0);
        e2 = transform_point(rotation, e2);
        Vector3f e3(0, 1, 0);
        e3 = transform_point(rotation, e3);

        Ray camera_ray;
        // setup camera location vector
        Vector3f e = camera.getPosition();
        camera_ray.origin_x = e(0);      
        camera_ray.origin_y = e(1);     
        camera_ray.origin_z = e(2);

        int numLights = scene.lights.size();

        float h = 2 * near * tan(camera.getFov() * M_PI / 180 / 2);
        float w = camera.getAspect() * h;

        for (int x = 0; x < XRES; x++) {
            for (int y = 0; y < YRES; y++) {

                //cout << to_string(x) << " " << to_string(y) << endl;

                float xi = (x - 1.0 * XRES / 2) * (w / XRES);
                float yi = (y - 1.0 * YRES / 2) * (h / YRES);

                Vector3f a = near * e1 + xi * e2 + yi * e3;

                camera_ray.direction_x = a(0);
                camera_ray.direction_y = a(1);
                camera_ray.direction_z = a(2);

                Ray point = findIntersection(camera_ray);

                if(point.isempty == false){
                    Vector3f diffuse_sum = {0, 0, 0};
                    Vector3f specular_sum = {0, 0, 0};

                    Vector3f prmcolor(point.prm -> getColor().r, point.prm -> getColor().g, point.prm -> getColor().b);

                    Vector3f amb = point.prm -> getAmbient() * prmcolor;
                    Vector3f diff = point.prm -> getDiffuse() * prmcolor;
                    Vector3f spec = point.prm -> getSpecular() * prmcolor;

                    Vector3f p(point.origin_x, point.origin_y, point.origin_z);
                    Vector3f n(point.direction_x, point.direction_y, point.direction_z);
                    Vector3f edir(camera_ray.origin_x - point.origin_x, camera_ray.origin_y - point.origin_y, camera_ray.origin_z -point.origin_z);
                    edir.normalize();
                    
                    for(int i = 0; i < numLights; i++){
                        Vector3f lp(scene.lights[i].position[0], scene.lights[i].position[1], scene.lights[i].position[2]);
                        //////cout << "lp: " << lp << endl;
                        Vector3f ltp = (p - lp).normalized();
                        //////cout << "ltp: " << ltp << endl;
                        Ray ltp_ray;
                        ltp_ray.origin_x = lp(0);
                        ltp_ray.origin_y = lp(1);
                        ltp_ray.origin_z = lp(2);
                        ltp_ray.direction_x = ltp(0);
                        ltp_ray.direction_y = ltp(1);
                        ltp_ray.direction_z = ltp(2);

                        //cout << "check intersection for lights " << to_string(i) << endl;
                        Ray intersection_ray = findIntersection(ltp_ray);
                        if(intersection_ray.isempty == true){
                            //cout << "weird things happen." << endl;
                        }
                        if(intersection_ray.isempty == false && point.prm != intersection_ray.prm){

                            ////cout << "lighting faild for l" << to_string(i) << endl;
                            continue;
                        }

                        Vector3f temp_lc(scene.lights[i].color[0], scene.lights[i].color[1], scene.lights[i].color[2]);
                        Vector3f dist = lp - p;
                        Vector3f lc = temp_lc / (1 + scene.lights[i].k * dist.dot(dist));
                        Vector3f ldir = dist.normalized();
                        Vector3f ldiff = lc * max((float)0,  n.dot(ldir));
                        diffuse_sum += ldiff;
                        ////cout << diffuse_sum << endl;
                        Vector3f lspec = lc * pow(max((float)0, n.dot((edir+ldir).normalized())), point.prm -> getGloss());
                        specular_sum += lspec;
                        ////cout << specular_sum << endl;
                    }

                    Vector3f clamp;
                    clamp << 1, 1, 1;
                    Vector3f color = clamp.cwiseMin(amb + diffuse_sum.cwiseProduct(diff) + specular_sum.cwiseProduct(spec));
                    if(x == 25 && y == 14){
                    //cout << "color: " << color << endl;
                }
                    png.setPixel(x, y, color(0), color(1), color(2));
                }else{
                    ////cout << "no intersection point" << endl;
                    png.setPixel(x, y, 0, 0, 0);
                }
            }
        }

        // LEAVE THIS UNLESS YOU WANT TO WRITE YOUR OWN OUTPUT FUNCTION
        if (png.saveImage()) {
            fprintf(stderr, "Error: couldn't save PNG image\n");
        } else {
            printf("DONE!\n");
        }
        
    }

    float intersectionTest(Renderable *ren, vector<Transformation> &transformation_stack, Ray &camera_ray, vector<Ray> &rays){
        if(ren->getType() == PRM){

            Primitive* prm = dynamic_cast<Primitive*>(ren);

            Vector3f a(camera_ray.direction_x, camera_ray.direction_y, camera_ray.direction_z);
            Vector3f b(camera_ray.origin_x, camera_ray.origin_y, camera_ray.origin_z);

            Vector3f prm_scale = prm -> getCoeff();
            Vector4f scale;
            scale << prm_scale, 1;
            transformation_stack.push_back(Transformation(SCALE, scale));

            Matrix4f transmatrix = get_inv_normal_transform_matrix(transformation_stack);
            a = transform_point(transmatrix, a);
            transmatrix = get_inv_transform_matrix(transformation_stack);
            b = transform_point(transmatrix, b);

            vector<float> initial_guess = get_initial_guess(prm, a, b);

            int guess_size = initial_guess.size();
            float final_t = 0;

            // get final t value
            if(guess_size == 0){
                final_t = FLT_MAX;
            }else if(guess_size == 1){
                final_t = Newtons_method(prm, a, b, initial_guess[0]);
                if(final_t <= 0){final_t = FLT_MAX;}
            }else{
                float final_t1 = Newtons_method(prm, a, b, initial_guess[0]);
                float final_t2 = Newtons_method(prm, a, b, initial_guess[1]);
                if(final_t1 > 0 && final_t2 > 0){
                    final_t = min(final_t1, final_t2);
                }else{
                    final_t = FLT_MAX;
                }
            }

            // build intersection ray
            Ray intersection_ray;

            if(final_t == FLT_MAX){
                intersection_ray.isempty = true;
            } else{
                Vector3f ray_location = b + final_t * a;
                Vector3f ray_normal = grad_sq_io(prm->getExp0(), prm->getExp1(), ray_location);
                transmatrix = get_transform_matrix(transformation_stack);
                ray_location = transform_point(transmatrix, ray_location);
                transmatrix = get_normal_transform_matrix(transformation_stack);
                ray_normal = transform_point(transmatrix, ray_normal);
                double length = ray_normal.norm();
                ray_normal = ray_normal / length;
                //cout << "ray_location: " << ray_location << endl;
                //cout << "ray_normal: " << ray_normal << endl;
                intersection_ray.origin_x = ray_location(0);
                intersection_ray.origin_y = ray_location(1);
                intersection_ray.origin_z = ray_location(2);
                intersection_ray.direction_x = ray_normal(0);
                intersection_ray.direction_y = ray_normal(1);
                intersection_ray.direction_z = ray_normal(2);
                intersection_ray.isempty = false;
                b << camera_ray.origin_x, camera_ray.origin_y, camera_ray.origin_z;
                intersection_ray.dist = (ray_location - b).norm();
            }
            intersection_ray.prm = prm;
            rays[1] = intersection_ray;
            transformation_stack.pop_back();
            return final_t;
        }
        else if(ren->getType() == OBJ){
            Object *obj = dynamic_cast<Object*>(ren);
            const vector<Transformation>& overall_trans =
            obj->getOverallTransformation();
            for (int i = overall_trans.size() - 1; i >= 0; i--) {
                transformation_stack.push_back(overall_trans.at(i));
            }
            float final_t = FLT_MAX;
            for (auto& child_it : obj->getChildren()) {
                //printf("check child: %s\n", child_it.second.name.name);
                const vector<Transformation>& child_trans = 
                child_it.second.transformations;
                for (int i = child_trans.size() - 1; i >= 0; i--) {
                    transformation_stack.push_back(child_trans.at(i));
                }
                ////cout << "final_t: " << final_t << endl;
                float t_val = intersectionTest(Renderable::get(child_it.second.name), transformation_stack, camera_ray, rays);
                //cout << "t_val after test: " << t_val << endl;
                if(t_val > 0 && t_val < FLT_MAX && rays[1].isempty == false && rays[1].dist < rays[0].dist){
                    //printf("child: %s wins with dist %f \n", child_it.second.name.name, rays[1].dist);
                    final_t = t_val;
                    rays[0] = rays[1];
                    //printRay(rays[0]);
                    //printRay(rays[1]);
                }

                transformation_stack.erase(transformation_stack.end() - child_trans.size(), transformation_stack.end());
            }
            transformation_stack.erase(transformation_stack.end() - overall_trans.size(), transformation_stack.end());
            return final_t;
        }
        else {
            fprintf(stderr, "Renderer::draw ERROR invalid RenderableType %d\n",
                ren->getType());
            exit(1);
        }
        return FLT_MAX;

    }

    vector<float> get_initial_guess(Renderable *ren, Vector3f &a, Vector3f &b){

        float d_a = a.dot(a);
        float d_b = 2 * a.dot(b);
        float d_c = b.dot(b) - 3;

        vector<float> initial_guess;

        float discriminant = d_b * d_b - 4 * d_a * d_c;
        if(discriminant < 0){
            //invisible
        } else{
            float tplus = 2.0 * d_c / (-d_b - sqrt(discriminant));
            float tminus = (-d_b - sqrt(discriminant)) / 2 / d_a;
            if(tplus <= 0 && tminus <= 0){
                // invisible 
            } else if(tplus > 0 && tminus > 0){
                initial_guess.push_back(min(tminus, tplus));
            } else{
                initial_guess.push_back(tminus);
                initial_guess.push_back(tplus);
            }
        }
        return initial_guess;
    }

    float Newtons_method(Primitive *prm, Vector3f &a, Vector3f &b, float initial_guess){

        float final_t = initial_guess;
        float e = prm -> getExp0();
        float n = prm -> getExp1();
        float gt = 0, gt_deriv = 0;
        do{
            Vector3f location = b + final_t * a;
            gt = sq_io(e, n, location);
            Vector3f grad = grad_sq_io(e, n, location);
            gt_deriv = a.dot(grad);
            if(gt_deriv > 0){
                return FLT_MAX;
            }
            final_t -= gt / gt_deriv;
            if(gt_deriv == 0){
                if(gt > 0) {return FLT_MAX;}
                if(gt == 0){break;}
            } 
        }while(gt > 1.0 / (20 * XRES));
        return final_t;
    }

    float sq_io(float e, float n, Vector3f &point){

        float x = point(0);
        float y = point(1);
        float z = point(2);

        return powf(powf(x * x, 1.0 / e) + powf(y * y, 1.0 / e), e / n) + powf(z * z, 1.0 / n) - 1.0;
    }

    Vector3f grad_sq_io(float e, float n, Vector3f &point){

        float x = point(0);
        float y = point(1);
        float z = point(2);

        Vector3f normal;

        normal(0) = (x == 0.0) ? 0.0 : 2.0 * x * powf(x * x, 1.0 / e - 1.0) *
        powf(powf(x * x, 1.0 / e) + powf(y * y, 1.0 / e),
            e / n - 1.0) / n;
        normal(1) = (y == 0.0) ? 0.0 : 2.0 * y * powf(y * y, 1.0 / e - 1.0) *
        powf(powf(x * x, 1.0 / e) + powf(y * y, 1.0 / e),
            e / n - 1.0) / n;
        normal(2) = (z == 0.0) ? 0.0 : 2.0 * z * powf(z * z, 1.0 / n - 1.0) / n;

        return normal;
    }

    Matrix4f get_rotation_matrix(Vector3f &axis, float theta){
        double length = axis.norm();
        float ux = axis(0) / length;
        float uy = axis(1) / length;
        float uz = axis(2) / length;

        Matrix4f rotation;
        rotation << ux * ux + (1 - ux * ux) * cos(theta), ux * uy * (1 - cos(theta)) - uz * sin(theta), ux * uz * (1 - cos(theta)) + uy * sin(theta), 0, \
        uy * ux * (1 - cos(theta)) + uz * sin(theta), uy * uy + (1 - uy * uy) * cos(theta), uy * uz * (1 - cos(theta)) - ux * sin(theta), 0, \
        uz * ux * (1 - cos(theta)) - uy * sin(theta), uz * uy * (1 - cos(theta)) + ux * sin(theta), uz * uz + (1 - uz * uz) * cos(theta), 0, \
        0, 0, 0, 1;
        return rotation;
    }

    Matrix4f get_transform_matrix(vector<Transformation> &transformation_stack){
        int size = transformation_stack.size();
        Matrix4f transmatrix;
        transmatrix << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
        for(int i = 0; i < size; i++){
            Matrix4f m;
            float ux = transformation_stack[i].trans(0);
            float uy = transformation_stack[i].trans(1);
            float uz = transformation_stack[i].trans(2);
            float theta = transformation_stack[i].trans(3);

            switch(transformation_stack[i].type){
                case TRANS:
                m << 1, 0, 0, ux, 0, 1, 0, uy, 0, 0, 1, uz, 0, 0, 0, 1;
                break;
                case SCALE:
                m << ux, 0, 0, 0, 0, uy, 0, 0, 0, 0, uz, 0, 0, 0, 0, 1;
                break;
                case ROTATE:
                Vector3f axis(ux, uy, uz);
                m = get_rotation_matrix(axis, theta);
                break;
            }
            transmatrix = transmatrix * m;
        }
        return transmatrix;
    }
    
    Matrix4f get_normal_transform_matrix(vector<Transformation> &transformation_stack){
        int size = transformation_stack.size();
        Matrix4f transmatrix;
        transmatrix << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
        for(int i = 0; i < size; i++){
            Matrix4f m;
            float ux = transformation_stack[i].trans(0);
            float uy = transformation_stack[i].trans(1);
            float uz = transformation_stack[i].trans(2);
            float theta = transformation_stack[i].trans(3);

            if(transformation_stack[i].type == SCALE){
                m << ux, 0, 0, 0, 0, uy, 0, 0, 0, 0, uz, 0, 0, 0, 0, 1;
                transmatrix = transmatrix * m;
            }
            if(transformation_stack[i].type == ROTATE){
                Vector3f axis(ux, uy, uz);
                m = get_rotation_matrix(axis, theta);
                transmatrix = transmatrix * m;
            }
        }
        return transmatrix.inverse().transpose();
    }

    Matrix4f get_inv_transform_matrix(vector<Transformation> &transformation_stack){
        int size = transformation_stack.size();
        Matrix4f transmatrix;
        transmatrix << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
        for(int i = size - 1; i > -1; i--){
            Matrix4f m;
            float ux = transformation_stack[i].trans(0);
            float uy = transformation_stack[i].trans(1);
            float uz = transformation_stack[i].trans(2);
            float theta = transformation_stack[i].trans(3);

            switch(transformation_stack[i].type){
                case TRANS:
                m << 1, 0, 0, -ux, 0, 1, 0, -uy, 0, 0, 1, -uz, 0, 0, 0, 1;
                break;
                case SCALE:
                m << 1.0 / ux, 0, 0, 0, 0, 1.0 / uy, 0, 0, 0, 0, 1.0 / uz, 0, 0, 0, 0, 1;
                break;
                case ROTATE:
                Vector3f axis(ux, uy, uz);
                m = get_rotation_matrix(axis, theta);
                m.transposeInPlace();
                break;
            }
            transmatrix = transmatrix * m;
        }
        return transmatrix;
    }

    Matrix4f get_inv_normal_transform_matrix(vector<Transformation> &transformation_stack){
        int size = transformation_stack.size();
        Matrix4f transmatrix;
        transmatrix << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
        for(int i = size - 1; i > -1; i--){
            Matrix4f m;
            float ux = transformation_stack[i].trans(0);
            float uy = transformation_stack[i].trans(1);
            float uz = transformation_stack[i].trans(2);
            float theta = transformation_stack[i].trans(3);
            if(transformation_stack[i].type == SCALE){
                m << 1.0 / ux, 0, 0, 0, 0, 1.0 / uy, 0, 0, 0, 0, 1.0 / uz, 0, 0, 0, 0, 1;
                transmatrix = transmatrix * m;
            }
            if(transformation_stack[i].type == ROTATE){
                Vector3f axis(ux, uy, uz);
                m = get_rotation_matrix(axis, theta);
                m.transposeInPlace();
                transmatrix = transmatrix * m;
            }
        }
        return transmatrix;
    }
    
    
    Vector3f transform_point(Matrix4f &transmatrix, Vector3f &point){
        Vector4f temp_point(point(0), point(1), point(2), 1);
        temp_point = transmatrix * temp_point;
        return Vector3f(temp_point(0) / temp_point(3), temp_point(1) / temp_point(3), temp_point(2) / temp_point(3));
    }

    void printRay(Ray &ray){
        cout << "ray location: " + to_string(ray.origin_x) + " " + to_string(ray.origin_y) + " " + to_string(ray.origin_z) << endl;
        cout << "ray normal: " + to_string(ray.direction_x) + " " + to_string(ray.direction_y) + " " + to_string(ray.direction_z) << endl;
        printf("dist: %f\n", ray.dist);
        printf("isempty: %d\n", ray.isempty);
    }
};
