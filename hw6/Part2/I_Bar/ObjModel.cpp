#include "ObjModel.h"

using namespace std;

ObjModel::ObjModel(){

}
ObjModel::~ObjModel(){

}

int ObjModel::parse(char * filename){
	if(filename){	
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return -1;
		}

		vector<string> elems;
		vector<string> light;
		string str;

		getline(inf, str);
		total_frames = stoi(str);

		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			int size = elems.size();
			if(size == 2){
				addFrame(stoi(elems[1]));
			}else if(size == 4){
				if(elems[0] == "translation"){
					addTranslation(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));
				}else if(elems[0] == "scale"){
					addScale(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));
				}
			}
			else if(size == 5){
				addRotation(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL), stof(elems[4], NULL));
			}
		}
		return 0;
	}
	return -1;	
}

void ObjModel::addFrame(int index){
	Frame frame;
	frame.index = index;
	m_frames.push_back(frame);
}

void ObjModel::addTranslation(float x, float y, float z){
	int size  = m_frames.size();
	Transform m = Transform(x, y, z, 0, 't');
	m_frames[size-1].trans.push_back(m);
}
void ObjModel::addRotation(float x, float y, float z, float theta){
	int size  = m_frames.size();
	float angle = theta / 360 * M_PI;
	Transform m = Transform(cos(angle), x * sin(angle), y * sin(angle), z * sin(angle), 'r');
	m_frames[size-1].trans.push_back(m);
}

void ObjModel::addScale(float x, float y, float z){
	int size  = m_frames.size();
	Transform m = Transform(x, y, z, 0, 's');
	m_frames[size-1].trans.push_back(m);
}

void ObjModel::printObjModelInfo(void){
	int	size = m_frames.size();
	for(int i = 0; i < size; i++){
		cout << "frame index: " << to_string(m_frames[i].index) << endl;
		int transize = m_frames[i].trans.size();
		for(int j = 0; j < transize; j++){
			if(m_frames[i].trans[j].type == 't'){
				cout << "translation " + to_string(m_frames[i].trans[j].x) + " " + to_string(m_frames[i].trans[j].y) + " " + to_string(m_frames[i].trans[j].z) << endl;
			}
			if(m_frames[i].trans[j].type == 's'){
				cout << "scale " + to_string(m_frames[i].trans[j].x) + " " + to_string(m_frames[i].trans[j].y) + " " + to_string(m_frames[i].trans[j].z) << endl;
			}
			if(m_frames[i].trans[j].type == 'r'){
				cout << "rotation " + to_string(m_frames[i].trans[j].x) + " " + to_string(m_frames[i].trans[j].y) + " " + to_string(m_frames[i].trans[j].z) << endl;
			}
		}
	}
}

vector<string> ObjModel::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}
