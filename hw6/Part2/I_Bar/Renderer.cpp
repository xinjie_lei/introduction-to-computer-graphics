#include "Renderer.h"

using namespace std;
using namespace Eigen;

Renderer::Renderer(){

}
Renderer::~Renderer(){

}

void Renderer::setsize(int xres, int yres){
	this -> xres = xres;
	this -> yres = yres;
}

int Renderer::parse(char * filename){
	m_model = ObjModel();
	if(m_model.parse(filename) !=0){
		cerr << "can't parse file." << endl;
		return -1;
	}
    current_frame = 0;
    return 0;
}


void Renderer::init(void){

	// enable Gouraud shading 
	glShadeModel(GL_SMOOTH);

	// enable Backface culling
	glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    // enable Depth buffering
    glEnable(GL_DEPTH_TEST);

    // enable automatic normalization
    glEnable(GL_NORMALIZE);
    
    // instead of using vertex and normal array, use quadratic object
    quadratic = gluNewQuadric();

    // set main matrix to projection matrix (for convertng camera to ndc)
    glMatrixMode(GL_PROJECTION);

    // set projection matrix based on frustum in world space
    glLoadIdentity();
    glFrustum(-1.0, 1.0,
      -1.0, 1.0,
      1.0, 60.0);

    // set main matrix to modelview matrix (for converting world to camera)
    glMatrixMode(GL_MODELVIEW);

    // initilize rotation quaternions
    lastrot << 1, 0, 0, 0;
    currrot << 1, 0, 0, 0;
}


void Renderer::display(void){

	// clear pixel grid and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// initialize ModelView matrix to identity
	glLoadIdentity();

	// set inverse camera transformation matrix

	glTranslatef(0, 0, -40);

	//update I_bar
	if(current_frame != 0){
		update_IBar();
	}

	// draw all objects on canvas
	drawIBar();

	// swap active and off-screen buffers
	glutSwapBuffers();
}

void Renderer::drawIBar(){

    /* Parameters for drawing the cylinders */
    float cyRad = 0.2, cyHeight = 1.0;
    int quadStacks = 4, quadSlices = 4;
    
    glPushMatrix();
    glColor3f(0, 0, 1);
    glTranslatef(0, cyHeight, 0);
    glRotatef(90, 1, 0, 0);
    gluCylinder(quadratic, cyRad, cyRad, 2.0 * cyHeight, quadSlices, quadStacks);
    glPopMatrix();
    
    glPushMatrix();
    glColor3f(0, 1, 1);
    glTranslatef(0, cyHeight, 0);
    glRotatef(90, 0, 1, 0);
    gluCylinder(quadratic, cyRad, cyRad, cyHeight, quadSlices, quadStacks);
    glPopMatrix();
    
    glPushMatrix();
    glColor3f(1, 0, 1);
    glTranslatef(0, cyHeight, 0);
    glRotatef(-90, 0, 1, 0);
    gluCylinder(quadratic, cyRad, cyRad, cyHeight, quadSlices, quadStacks);
    glPopMatrix();
    
    glPushMatrix();
    glColor3f(1, 1, 0);
    glTranslatef(0, -cyHeight, 0);
    glRotatef(-90, 0, 1, 0);
    gluCylinder(quadratic, cyRad, cyRad, cyHeight, quadSlices, quadStacks);
    glPopMatrix();
    
    glPushMatrix();
    glColor3f(0, 1, 0);
    glTranslatef(0, -cyHeight, 0);
    glRotatef(90, 0, 1, 0);
    gluCylinder(quadratic, cyRad, cyRad, cyHeight, quadSlices, quadStacks);
    glPopMatrix();
}

void Renderer::reshape(int width, int height){

    width = (width == 0) ? 1 : width;
    height = (height == 0) ? 1 : height;

    // enable NDC to scene conversion with xres and yres
    glViewport(0, 0, width, height);

    mouse_scale_x = (float) (1 + 1) / (float) width;
    mouse_scale_y = (float) (1 + 1) / (float) height;

    // enable redisplay if size changed
    glutPostRedisplay();
}

void Renderer::key_pressed(unsigned char key, int x, int y)
{   
    if(key == 'q'){
        exit(0);
    }
    if(key == 'n'){
        if(current_frame < m_model.total_frames){
            current_frame = (current_frame + 1) % m_model.total_frames;
            glutPostRedisplay();
        }
    }
}

RowVector4i Renderer::get_four_neighbor_indices(int index){
    int i = 0;
    RowVector4i v;
    v << 0, 0, 0, 0;
    while(index > m_model.m_frames[i].index){
        if(i == m_model.m_frames.size() - 1){
            v << i-1, i, 0, 1;
            break;
        }
        else if(index <= m_model.m_frames[i+1].index){
           if(i == 0){
               v << m_model.m_frames.size()-1, 0, 1, 2;
           }else if(i == m_model.m_frames.size() - 2){
              v << i-1, i, i+1, 0;
          }else{
              v << i-1, i, i+1, i+2;
          }
          break;
      }
      i++;
  } 
  return v;
}

RowVector4f Renderer::get_u_vector(RowVector4i indices){

    float u_value = 0;
    RowVector4f u;
    if(indices(2) == 0){
        u_value = 1.0 * (current_frame - m_model.m_frames[indices(1)].index) / (m_model.total_frames - m_model.m_frames[indices(1)].index); 
    }else{
        u_value = 1.0 * (current_frame - m_model.m_frames[indices(1)].index) / (m_model.m_frames[indices(2)].index - m_model.m_frames[indices(1)].index);
    }
    u << 1, u_value, u_value * u_value, u_value * u_value * u_value;
    return u;
}

void Renderer::update_IBar(void){
    RowVector4i indices = get_four_neighbor_indices(current_frame);
    RowVector4f u = get_u_vector(indices);
    Matrix4f basis;
    basis << 0, 1, 0, 0, -0.5, 0, 0.5, 0, 1, -2.5, 2, -0.5, -0.5, 1.5, -1.5, 0.5;

    Vector4f p;
    float x, y, z, angle;
    // translation x
    p << m_model.m_frames[indices(0)].trans[0].x, m_model.m_frames[indices(1)].trans[0].x, m_model.m_frames[indices(2)].trans[0].x, m_model.m_frames[indices(3)].trans[0].x;
    x = u * basis * p;

    // translation y
    p << m_model.m_frames[indices(0)].trans[0].y, m_model.m_frames[indices(1)].trans[0].y, m_model.m_frames[indices(2)].trans[0].y, m_model.m_frames[indices(3)].trans[0].y;
    y = u * basis * p;

    // translation z
    p << m_model.m_frames[indices(0)].trans[0].z, m_model.m_frames[indices(1)].trans[0].z, m_model.m_frames[indices(2)].trans[0].z, m_model.m_frames[indices(3)].trans[0].z;
    z = u * basis * p;
    glTranslatef(x, y, z);

    // scale x
    p << m_model.m_frames[indices(0)].trans[1].x, m_model.m_frames[indices(1)].trans[1].x, m_model.m_frames[indices(2)].trans[1].x, m_model.m_frames[indices(3)].trans[1].x;
    x = u * basis * p;

    // scale y
    p << m_model.m_frames[indices(0)].trans[1].y, m_model.m_frames[indices(1)].trans[1].y, m_model.m_frames[indices(2)].trans[1].y, m_model.m_frames[indices(3)].trans[1].y;
    y = u * basis * p;

    // scale z
    p << m_model.m_frames[indices(0)].trans[1].z, m_model.m_frames[indices(1)].trans[1].z, m_model.m_frames[indices(2)].trans[1].z, m_model.m_frames[indices(3)].trans[1].z;
    z = u * basis * p;
    glScalef(x, y, z);

    // quoternion s
    p << m_model.m_frames[indices(0)].trans[2].x, m_model.m_frames[indices(1)].trans[2].x, m_model.m_frames[indices(2)].trans[2].x, m_model.m_frames[indices(3)].trans[2].x;
    angle = u * basis * p;

    // quoternion v.x
    p << m_model.m_frames[indices(0)].trans[2].y, m_model.m_frames[indices(1)].trans[2].y, m_model.m_frames[indices(2)].trans[2].y, m_model.m_frames[indices(3)].trans[2].y;
    x = u * basis * p;

    // quoternion v.y
    p << m_model.m_frames[indices(0)].trans[2].z, m_model.m_frames[indices(1)].trans[2].z, m_model.m_frames[indices(2)].trans[2].z, m_model.m_frames[indices(3)].trans[2].z;
    y = u * basis * p;

    // quoternion v.z
    p << m_model.m_frames[indices(0)].trans[2].w, m_model.m_frames[indices(1)].trans[2].w, m_model.m_frames[indices(2)].trans[2].w, m_model.m_frames[indices(3)].trans[2].w;
    z = u * basis * p;

    float norm = sqrt(angle * angle + x * x + y * y + z * z);
    angle /= norm;
    x /= norm;
    y /= norm;
    z /= norm;
    glRotatef(2*acos(angle) * 180 / M_PI, x / sqrt(1-angle * angle), y / sqrt(1-angle * angle), z / sqrt(1-angle * angle));
}
