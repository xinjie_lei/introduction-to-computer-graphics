#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <math.h>
#include <vector>
#include <iostream>
#include <cmath>
#include <sstream>
#include <string>
#include <fstream>

#define _USE_MATH_DEFINES

typedef struct _Transform_{
	float x;
	float y;
	float z;
	float w;
	char type;
	_Transform_(): x(0), y(0), z(0), w(0), type('N') {}
	_Transform_(float x, float y, float z, float w, char type): x(x), y(y), z(z), w(w), type(type) {}
	~_Transform_() {}
} Transform;

typedef struct _Frame_{
	int index;
	std::vector<Transform> trans;
} Frame;

class ObjModel{

	friend class Renderer;

	int total_frames;
	std::vector<Frame> m_frames;

public:
	ObjModel();
	~ObjModel();

	int parse(char * filename);
	void addFrame(int index);
	void addTranslation(float x, float y, float z);
	void addRotation(float x, float y, float z, float theta);
	void addScale(float x, float y, float z);
	void printObjModelInfo(void);
	std::vector<std::string> split(const std::string &s, char delim);

};



#endif
