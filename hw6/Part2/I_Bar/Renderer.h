#ifndef RENDERER_H
#define RENDERER_H

#include <math.h>
#include <iostream>
#include <functional>

#include <GL/glew.h>
#include <GL/glut.h>
#include <Eigen/Dense>
#include "ObjModel.h"

#define _USE_MATH_DEFINES

class Renderer{

	ObjModel m_model;
	int xres;
	int yres;
	int current_frame;
	int mouse_x;
	int mouse_y;
	float mouse_scale_x;
	float mouse_scale_y;
	bool is_pressed;

	Eigen::Vector4f lastrot;
	Eigen::Vector4f currrot;

	GLUquadricObj *quadratic;

public:
	Renderer();
	~Renderer();
	void setsize(int xres, int yres);
	int parse(char * filename);

	void init(void);
	void reshape(int xres, int yres);
	void display(void);

	void drawIBar(void);

	void mouse_pressed(int button, int state, int x, int y);
	void mouse_moved(int x, int y);
	void mouse_released(int button, int state, int x, int y);
	void key_pressed(unsigned char key, int x, int y);

	Eigen::RowVector4i get_four_neighbor_indices(int index);
	Eigen::RowVector4f get_u_vector(Eigen::RowVector4i indices);
	void update_IBar(void);
	
};

#endif
