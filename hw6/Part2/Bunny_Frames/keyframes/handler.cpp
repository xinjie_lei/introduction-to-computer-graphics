#include "ObjModel.h"

using namespace std;


int main(int argc, char * argv[]){
	
	if(argc != 1){
		cout << "usage: ./interpolate \n" << endl;
		return -1;
	}

	ObjModel model;
	model.parse();
	model.interpolate();
	return 0;
}
	

