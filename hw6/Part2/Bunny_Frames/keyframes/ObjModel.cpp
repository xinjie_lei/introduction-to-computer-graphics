#include "ObjModel.h"

using namespace std;
using namespace Eigen;

ObjModel::ObjModel(){
	m_filenames.push_back("bunny00.obj");
	m_filenames.push_back("bunny05.obj");
	m_filenames.push_back("bunny10.obj");
	m_filenames.push_back("bunny15.obj");
	m_filenames.push_back("bunny20.obj");
    m_basis << 0, 1, 0, 0, -0.5, 0, 0.5, 0, 1, -2.5, 2, -0.5, -0.5, 1.5, -1.5, 0.5;

}

ObjModel::~ObjModel(){

}

void ObjModel::parse(void){
	for(int i = 0; i < 5; i++){	
		ifstream inf(m_filenames[i]);
		if(!inf){
			cerr << "can't open file." << m_filenames[i] << endl;
			return;
		}

		vector<string> elems;
		string str;
		Frame frame;
		frame.index = i * 5;

		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			if(elems.size() == 4){
				if(elems[0] == "v"){
					frame.vertices.push_back(Triple(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL)));
				}else{
					if(i == 0){
						m_faces.push_back(Face(stoi(elems[1], NULL), stoi(elems[2], NULL), stoi(elems[3], NULL)));
					}
				}
			}
		}
		m_frames.push_back(frame);
	}
	//printObjModelInfo();
}

void ObjModel::printObjModelInfo(void){
	int	size = m_frames.size();
	for(int i = 0; i < size; i++){
		cout << "frame index: " << to_string(m_frames[i].index) << endl;
		int transize = m_frames[i].vertices.size();
		for(int j = 0; j < transize; j++){
			cout << "v " + to_string(m_frames[i].vertices[j].x) + " " + to_string(m_frames[i].vertices[j].y) + " " + to_string(m_frames[i].vertices[j].z) << endl;
		}
	}
}

RowVector4f ObjModel::get_u_vector(int index, RowVector4i indices){
    
    float u_value = 0;
    RowVector4f u;
    u_value = 1.0 * (index - m_frames[indices(1)].index) / (m_frames[indices(2)].index - m_frames[indices(1)].index);
    u << 1, u_value, u_value * u_value, u_value * u_value * u_value;
    //cout << "u_vector:" << u << endl;
    return u;
}


RowVector4i ObjModel::get_four_neighbor_indices(int index){
    RowVector4i v;
    if(index > 0 && index < 5){
    	v << 0, 0, 1, 2;
    }
    else if(index > 5 && index < 10){
    	v << 0, 1, 2, 3;
    }
    else if(index > 10 && index < 15){
    	v << 1, 2, 3, 4;
    }
    else if(index > 15 && index < 20){
    	v << 2, 3, 4, 4;
    }
    //cout << "index: " << index << " neighbor indices: " << v << endl; 
    return v;
}

RowVector3f ObjModel::interpolate_vertex(RowVector4i indices, RowVector4f u, int ind){
    Vector4f p;
    float x, y, z;
    // translation x
    p << m_frames[indices(0)].vertices[ind].x, m_frames[indices(1)].vertices[ind].x, m_frames[indices(2)].vertices[ind].x, m_frames[indices(3)].vertices[ind].x;
    x = u * m_basis * p;

    // translation y
    p << m_frames[indices(0)].vertices[ind].y, m_frames[indices(1)].vertices[ind].y, m_frames[indices(2)].vertices[ind].y, m_frames[indices(3)].vertices[ind].y;
    y = u * m_basis * p;

    // translation z
    p << m_frames[indices(0)].vertices[ind].z, m_frames[indices(1)].vertices[ind].z, m_frames[indices(2)].vertices[ind].z, m_frames[indices(3)].vertices[ind].z;
    z = u * m_basis * p;
    //cout << "update translation: " << x << " " << y << " " << z << endl;
    RowVector3f res;
    res << x, y, z;
    return res;
}

void ObjModel::interpolate(void){
	for(int index = 1; index < 20; index++){
		if(index != 5 && index != 10 && index != 15){
			stringstream ss;
			if(index < 10){
				ss << "bunny0" << index << ".obj";
			}else{
				ss << "bunny" << index << ".obj";
			}
			ofstream outf(ss.str());
			RowVector4i indices = get_four_neighbor_indices(index);
			RowVector4f u = get_u_vector(index, indices);
			int size = m_frames[0].vertices.size();
			for(int j = 0; j < size; j++){
				RowVector3f res = interpolate_vertex(indices, u, j);
				outf << "v " << to_string(res(0)) << " " << to_string(res(1)) << " " << to_string(res(2)) << endl;
			}
			size = m_faces.size();
			for(int j = 0; j < size; j++){
				outf << "f " << to_string(m_faces[j].x) << " " << to_string(m_faces[j].y) << " " << to_string(m_faces[j].z) << endl;
			}
			outf.close();
		}
	}
}

vector<string> ObjModel::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}