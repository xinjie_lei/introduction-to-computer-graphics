#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <math.h>
#include <vector>
#include <iostream>
#include <cmath>
#include <sstream>
#include <string>
#include <fstream>
#include <Eigen/Dense>

#define _USE_MATH_DEFINES

typedef struct _Triple_{
	float x;
	float y;
	float z;
	_Triple_(): x(0), y(0), z(0) {}
	_Triple_(float x, float y, float z): x(x), y(y), z(z) {}
	~_Triple_() {}
} Triple;

typedef struct _Face_{
	int x;
	int y;
	int z;
	_Face_(): x(0), y(0), z(0) {}
	_Face_(int x, int y, int z): x(x), y(y), z(z) {}
	~_Face_() {}
} Face;

typedef struct _Frame_{
	int index;
	std::vector<Triple> vertices;
} Frame;

class ObjModel{

	friend class Renderer;

	int total_frames;
	std::vector<Frame> m_frames;
	std::vector<std::string> m_filenames;
	std::vector<Face> m_faces;
	Eigen::Matrix4f m_basis;

public:
	ObjModel();
	~ObjModel();

	void parse(void);
	void printObjModelInfo(void);
	Eigen::RowVector4f get_u_vector(int index, Eigen::RowVector4i indices);
	Eigen::RowVector4i get_four_neighbor_indices(int index);
	Eigen::RowVector3f interpolate_vertex(Eigen::RowVector4i indices, Eigen::RowVector4f u, int ind);
	void interpolate(void);
	std::vector<std::string> split(const std::string &s, char delim);

};



#endif
