#include "Renderer.h"

using namespace std;

Renderer * rendererPtr;
void display();
void reshape(int xres, int yres);
void key_pressed(unsigned char key, int x, int y);
void mouse_pressed(int button, int state, int x, int y);
void mouse_moved(int x, int y);

int main(int argc, char * argv[]){
	
	if(argc < 5){
		cout << "usage: ./wireframe file.txt xres yres h\n" << endl;
		return -1;
	}
	Renderer renderer;
	rendererPtr = &renderer;

	int xres = stoi(argv[2]);
	int yres = stoi(argv[3]);

	// set image size and parse iuput file
	renderer.setsize(xres, yres);
	renderer.parse(argv[1], stof(argv[4]));

	glutInit(&argc, argv);

	// enable double buffer
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

	// initialize buffer size 
	glutInitWindowSize(xres, yres);

	// initialize window location
	glutInitWindowPosition(0, 0);

	// name the program window "Test".
    glutCreateWindow("Test");

    // set up OpenGL environment
    renderer.init();
    // specify display function.
    glutDisplayFunc(display);

    // specify reshape function.
    glutReshapeFunc(reshape);

	// handle mouse press
    glutMouseFunc(mouse_pressed);

    // handle mouse movement.
    glutMotionFunc(mouse_moved);

    // handle keyboard press
	glutKeyboardFunc(key_pressed);
	
    // start event processing loop
    glutMainLoop();
}

void display(void){
	rendererPtr -> display();
}

void reshape(int xres, int yres){
	rendererPtr -> reshape(xres, yres);
}

void mouse_pressed(int button, int state, int x, int y){
	rendererPtr -> mouse_pressed(button, state, x, y);
}
void mouse_moved(int x, int y){
	rendererPtr -> mouse_moved(x, y);
}

void key_pressed(unsigned char key, int x, int y){
	rendererPtr -> key_pressed(key, x, y);
}
