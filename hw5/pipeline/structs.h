#ifndef STRUCTS_H
#define STRUCTS_H

#include <vector>

struct Vec3f
{
	float x, y, z;
	Vec3f(): x(0), y(0), z(0) {}
	Vec3f(float x, float y, float z): x(x), y(y), z(z) {}
	~Vec3f() {}
};

struct Vertex
{
    float x, y, z;
    Vertex(): x(0), y(0), z(0) {}
	Vertex(float x, float y, float z): x(x), y(y), z(z) {}
	~Vertex() {}
};

struct Face
{
    int idx1, idx2, idx3;
    Face(): idx1(0), idx2(0), idx3(0) {}
	Face(int x, int y, int z): idx1(x), idx2(y), idx3(z) {}
	~Face() {}
};

struct Mesh_Data
{
    std::vector<Vertex> vertices;
    std::vector<Face> faces;
    std::vector<Vec3f> normals;
};

#endif
