#include "ObjModel.h"

using namespace std;
using namespace Eigen;

ObjModel::ObjModel(){
}

ObjModel::~ObjModel(){
}

ObjModel::ObjModel(const ObjModel* source){
	name = source -> name;
	h = source -> h;

	int size = (source -> mesh).vertices.size();
	for(int i = 0; i < size; i++){
    	mesh.vertices.push_back((source -> mesh).vertices[i]);
	}

	size = (source -> mesh).faces.size();
	for(int i = 0; i < size; i++){
		mesh.faces.push_back((source -> mesh).faces[i]);
	}

}

void ObjModel::setNameAndH(const string s, float timestep){
	name = s;
	h = timestep;
}

string ObjModel::getName(){
	return name;
}

void ObjModel::addVertex(float x, float y, float z){
	Vertex v = Vertex(x, y, z);
	mesh.vertices.push_back(v);
}

void ObjModel::addFace(int idx1, int idx2, int idx3){
	Face f = Face(idx1, idx2, idx3);
	mesh.faces.push_back(f);
}

void ObjModel::computeNormal(void){
	hevs = new vector<HEV*>();
	hefs = new vector<HEF*>();
	area_sum.assign(mesh.vertices.size(), 0);
	build_HE(&mesh, hevs, hefs);
	compute_normals(hevs, area_sum); 
	mesh.normals.push_back(Vec3f(0, 0, 0));
	int size = hevs -> size();
	for(int i = 1; i < size; i++){
		mesh.normals.push_back(hevs -> at(i) -> normal);
	}
}
void ObjModel::smooth(void){
	solve(hevs, area_sum, h);
	int num_vertices = hevs -> size();
	for(int i = 1; i < num_vertices; i++){
		HEV* v = hevs -> at(i);
		mesh.vertices[i].x = v -> x;
		mesh.vertices[i].y = v -> y;
		mesh.vertices[i].z = v -> z;
		mesh.normals[i] = v -> normal;
	}
}
void ObjModel::setupVN(void){
	int size = mesh.faces.size();
	for(int i = 0; i < size; i++){
		vertices.push_back(mesh.vertices[mesh.faces[i].idx1]);
		normals.push_back(mesh.normals[mesh.faces[i].idx1]);
		vertices.push_back(mesh.vertices[mesh.faces[i].idx2]);
		normals.push_back(mesh.normals[mesh.faces[i].idx2]);
		vertices.push_back(mesh.vertices[mesh.faces[i].idx3]);
		normals.push_back(mesh.normals[mesh.faces[i].idx3]);
	}
}

void ObjModel::resetVN(void){
	int size = mesh.faces.size();
	int ind = 0;
	for(int i = 0; i < size; i++){
		vertices[ind] = mesh.vertices[mesh.faces[i].idx1];
		normals[ind] = mesh.normals[mesh.faces[i].idx1];
		vertices[ind+1] = mesh.vertices[mesh.faces[i].idx2];
		normals[ind+1] = mesh.normals[mesh.faces[i].idx2];
		vertices[ind+2] = mesh.vertices[mesh.faces[i].idx3];
		normals[ind+2] = mesh.normals[mesh.faces[i].idx3];
		ind += 3;
	}
}

void ObjModel::setTranslation(float x, float y, float z){
	TransformMat m = TransformMat(x, y, z, 0, 't');
	m_matrices.push_back(m);
}
void ObjModel::setRotation(float x, float y, float z, float theta){
	TransformMat m = TransformMat(x, y, z, theta, 'r');
	m_matrices.push_back(m);
}

void ObjModel::setScale(float x, float y, float z){
	TransformMat m = TransformMat(x, y, z, 0, 's');
	m_matrices.push_back(m);
}

void ObjModel::setAmbient(float x, float y, float z){
	amb[0] = x;
	amb[1] = y;
	amb[2] = z;
}
void ObjModel::setDiffuse(float x, float y, float z){
	diff[0] = x;
	diff[1] = y;
	diff[2] = z;
}

void ObjModel::setSpecular(float x, float y, float z){
	spec[0] = x;
	spec[1] = y;
	spec[2] = z;
}

void ObjModel::setShine(float x){
	shine = x;
}

void ObjModel::printObjModelInfo(void){
	int	size = mesh.vertices.size();
	for(int i = 0; i < size; i++){
		cout << "v " + to_string(mesh.vertices[i].x) + " " + to_string(mesh.vertices[i].y) + " " + to_string(mesh.vertices[i].z) << endl;
	}

	size = mesh.normals.size();
	for(int i = 0; i < size; i++){
		cout << "vn " + to_string(mesh.normals[i].x) + " " + to_string(mesh.normals[i].y) + " " + to_string(mesh.normals[i].z) << endl;
	}

	/*size = mesh.faces.size();
	for(int i = 0; i < size; i++){
		cout << "f " + to_string(mesh.faces[i].idx1) + " " + to_string(mesh.faces[i].idx2) + " " + to_string(mesh.faces[i].idx3) << endl;
	}*/
	size =  m_matrices.size();
	for(int i = 0; i < size; i++){
 		cout << "transform " + to_string(m_matrices[i].x) + " " + to_string(m_matrices[i].y) + " " + to_string(m_matrices[i].z) \
 		        + " " + to_string(m_matrices[i].angle) + " " + to_string(m_matrices[i].type) << endl;
 	}
	cout << "amb: " + to_string(amb[0]) + " " + to_string(amb[1]) + " " + to_string(amb[2]) << endl;
	cout << "diff: " + to_string(diff[0]) + " " + to_string(diff[1]) + " " + to_string(diff[2]) << endl;
	cout << "spec: " + to_string(spec[0]) + " " + to_string(spec[1]) + " " + to_string(spec[2]) << endl;
	cout << "shineness: " + to_string(shine) << endl;

	size = vertices.size();
	for(int i = 0; i < size; i++){
		cout << "v" + to_string(i) + " " + to_string(vertices[i].x) + " " + to_string(vertices[i].y) + " " + to_string(vertices[i].z) << endl;
		cout << "n" + to_string(i) + " " + to_string(normals[i].x) + " " + to_string(normals[i].y) + " " + to_string(normals[i].z) << endl;
	}
}
