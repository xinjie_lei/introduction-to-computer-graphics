#ifndef FILEMODEL_H
#define FILEMODEL_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

#include "ObjModel.h"

typedef struct _CameraPosition_{
	float cx;
	float cy;
	float cz;
	_CameraPosition_(): cx(0), cy(0), cz(0) {}
	_CameraPosition_(float cx, float cy, float cz): cx(cx), cy(cy), cz(cz) {}
	~_CameraPosition_() {}
} CPosition;

typedef struct _CameraOrientation_{
	float x;
	float y;
	float z;
	float angle;
	_CameraOrientation_(): x(0), y(0), z(0), angle(0) {}
	_CameraOrientation_(float x, float y, float z, float angle): x(x), y(y), z(z), angle(angle) {}
	~_CameraOrientation_() {}
	
} COrientation;

typedef struct _PerspectiveParameter_{
	float left;
	float right;
	float top;
	float bottom;
	float near;
	float far;
	_PerspectiveParameter_(): left(0), right(0), top(0), bottom(0), near(0), far(0) {}
	_PerspectiveParameter_(float left, float right, float top, float bottom, float near, float far): left(left), right(right), top(top), bottom(bottom), near(near), far(far) {}
	~_PerspectiveParameter_() {}
} PParameter;

typedef struct _Light_{
	float coord[4];
	float rgb[3];
	float kattn;
} Light;

class FileModel{

	friend class Renderer;

	CPosition m_cpos;
	COrientation m_cori;
	PParameter m_ppara;
	std::vector<ObjModel> m_copies;
	std::vector<Light> m_lights;

public:
	FileModel();
	~FileModel();
	int parse(char * filename, float h);
	int parseObjFile(std::vector<ObjModel> &initialmodels, const std::string modelname, const std::string &filename, float h);
	ObjModel* search(std::vector<ObjModel> &initialmodels, const std::string &s);
	std::vector<std::string> split(const std::string &s, char delim);
};

#endif