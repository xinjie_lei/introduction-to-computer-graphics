#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <vector>
#include <iostream>
#include <cmath>
#include "structs.h"
#include "halfedge.h"

typedef struct _TransformMat_{
	float x;
	float y;
	float z;
	float angle;
	char type;
	_TransformMat_(): x(0), y(0), z(0), angle(0), type('N') {}
	_TransformMat_(float x, float y, float z, float angle, char type): x(x), y(y), z(z), angle(angle), type(type) {}
	~_TransformMat_() {}
} TransformMat;

class ObjModel{
	friend class Renderer;

	std::string name;
	std::vector<TransformMat> m_matrices;
	Mesh_Data mesh;
	std::vector<Vertex> vertices;
	std::vector<Vec3f> normals;
	std::vector<HEV*> *hevs;
	std::vector<HEF*> *hefs;
	std::vector<double> area_sum;

	float amb[3];
	float diff[3];
	float spec[3];
	float shine;
	float h;

public:
	ObjModel();
	~ObjModel();
	ObjModel(const ObjModel* source);

	void setNameAndH(const std::string s, float h);
	std::string getName(void);

	void addVertex(float x, float y, float z);
	void addFace(int idx1, int idx2, int idx3);
	void computeNormal(void);
	void smooth(void);
	void resetVN(void);
	void setupVN(void);	
	void setTranslation(float x, float y, float z);
	void setRotation(float x, float y, float z, float theta);
	void setScale(float x, float y, float z);
	void setAmbient(float x, float y, float z);
	void setDiffuse(float x, float y, float z);
	void setSpecular(float x, float y, float z);
	void setShine(float x);

	void printObjModelInfo(void);
	std::vector<std::string> split(const std::string &s, char delim);

};



#endif