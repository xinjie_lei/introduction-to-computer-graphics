#ifndef RENDERER_H
#define RENDERER_H

#include <math.h>
#include <iostream>
#include <functional>

#include <GL/glew.h>
#include <GL/glut.h>
#include <Eigen/Dense>
#include "FileModel.h"

#define _USE_MATH_DEFINES

class Renderer{

	FileModel m_model;
	int xres;
	int yres;
	int mouse_x;
	int mouse_y;
	float mouse_scale_x;
	float mouse_scale_y;
	bool is_pressed;

	Eigen::Vector4d lastrot;
	Eigen::Vector4d currrot;

public:
	Renderer();
	~Renderer();
	void setsize(int xres, int yres);
	int parse(char * filename, float h);

	void init(void);
	void reshape(int xres, int yres);
	void display(void);

	void init_lights(void);
	void set_lights(void);
	void draw_objects(void);

	void key_pressed(unsigned char key, int x, int y);
	void mouse_pressed(int button, int state, int x, int y);
	void mouse_moved(int x, int y);
	void mouse_released(int button, int state, int x, int y);

	Eigen::Vector4d quat(int start, int curr, int x, int y);
	Eigen::Vector3d get_vector(int x, int y);
	Eigen::Matrix4d get_curr_rot(void);

	
};

#endif