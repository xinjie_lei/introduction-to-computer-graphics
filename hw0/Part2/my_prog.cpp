#include "TransformationParser.h"
#include "TransformationModel.h"
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

int main(int argc, char ** argv){
	
	if(argc < 2){
		cout << "usage: ./my_prog filename.txt\n" << endl;
		return -1;
	}

	TransformationParser parser;
	parser.parse(argv[1]);

	parser.printInverse();
	
	return 0;
}