#ifndef TRANSFORMATIONPARSER_H
#define TRANSFORMATIONPARSER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

#include "TransformationModel.h"

class TransformationParser{

	TransformationModel model;

public:
	TransformationParser();
	~TransformationParser();
	void parse(char* filename);
	void printInverse(void);
	std::vector<std::string> split(const std::string &s, char delim);
};

#endif