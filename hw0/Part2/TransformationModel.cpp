#include "TransformationModel.h"
using namespace Eigen;
using namespace std;

TransformationModel::TransformationModel(){

}
TransformationModel::~TransformationModel(){

}
void TransformationModel::setTranslation(double x, double y, double z){
	MatrixXd m(4, 4);
	m << 1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1;
	m_matrices.push_back(m);
}
void TransformationModel::setRotation(double x, double y, double z, double theta){
	double length = sqrt(x*x + y*y + z*z);
	double ux = x / length;
	double uy = y / length;
	double uz = z / length;

	MatrixXd m(4, 4);
	m << ux * ux + (1 - ux * ux) * cos(theta), ux * uy * (1 - cos(theta)) - uz * sin(theta), ux * uz * (1 - cos(theta)) + uy * sin(theta), 0, \
	     uy * ux * (1 - cos(theta)) + uz * sin(theta), uy * uy + (1 - uy * uy) * cos(theta), uy * uz * (1 - cos(theta)) - ux * sin(theta), 0, \
	     uz * ux * (1 - cos(theta)) - uy * sin(theta), uz * uy * (1 - cos(theta)) + ux * sin(theta), uz * uz + (1 - uz * uz) * cos(theta), 0, \
	     0, 0, 0, 1;
	m_matrices.push_back(m);
}

void TransformationModel::setScale(double x, double y, double z){
	MatrixXd m(4, 4);
	m << x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1;
	m_matrices.push_back(m);
}


void TransformationModel::computeProduct(void){
	int size = m_matrices.size();
	transmatrix = m_matrices[size - 1];

	for(int i = size - 2; i > -1; i--){
		transmatrix = transmatrix * m_matrices[i];
	}
}

MatrixXd TransformationModel::getProduct(void){
	return transmatrix;
}

MatrixXd TransformationModel::getInverse(void){
	return transmatrix.inverse();

}