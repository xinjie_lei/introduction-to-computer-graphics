#ifndef TRANSFORMATIONMODEL_H
#define TRANSFORMATIONMODEL_H

#include <iostream>
#include <vector>
#include <cmath>
#include <Eigen/Dense>

class TransformationModel{
	std::vector<Eigen::MatrixXd> m_matrices;
	Eigen::MatrixXd transmatrix;

public:
	TransformationModel();
	~TransformationModel();
	void setTranslation(double x, double y, double z);
	void setRotation(double x, double y, double z, double theta);
	void setScale(double x, double y, double z);
	void computeProduct(void);
	Eigen::MatrixXd getProduct(void);
	Eigen::MatrixXd getInverse(void);
};

#endif