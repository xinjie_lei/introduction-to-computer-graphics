#include "TransformationParser.h"

using namespace std;

TransformationParser::TransformationParser(){

}
TransformationParser::~TransformationParser(){

}

void TransformationParser::parse(char * filename){

	if(filename){	
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return;
		}

		vector<string> elems;
		string str;

		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			int size = elems.size();
			if(size == 4 || size == 5){
				if(elems[0] == "t"){
					model.setTranslation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
				if(elems[0] == "r"){
					model.setRotation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL), stod(elems[4], NULL));
				}
				if(elems[0] == "s"){
					model.setScale(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
			} 
		}
	}
}

void TransformationParser::printInverse(){
	model.computeProduct();
	cout << model.getInverse() << endl;
}
vector<string> TransformationParser::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}