#include "ObjParser.h"

using namespace std;

ObjParser::ObjParser(){

}

ObjParser::~ObjParser(){

}
int ObjParser::parse(char ** filename){
	int i = 1;

	while(filename[i]){
		
		ifstream inf(filename[i]);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename[i] << endl;
			return false;
		}

		ObjModel model;
		vector<string> elems;
		string str;
		model.setName(string(filename[i]));
		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			if(elems.size() == 4){
				if(elems[0] == "v"){
					model.addVertex(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				} else{
					model.addFace(stoi(elems[1], NULL), stoi(elems[2], NULL), stoi(elems[3], NULL));
				}
			}
		}
		m_models.push_back(model);
		i++;
	}

	return 0;
}

vector<string> ObjParser::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}

void ObjParser::printAll(void){
	int size = m_models.size();
	for(int i = 0; i < size; i++){
		m_models[i].printObjModelInfo();
	}
}