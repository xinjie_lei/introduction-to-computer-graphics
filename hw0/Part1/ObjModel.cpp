#include "ObjModel.h"

using namespace std;

ObjModel::ObjModel(){

}
ObjModel::~ObjModel(){

}

void ObjModel::setName(const string s){
	name = s;
}

string ObjModel::getName(){
	return name;
}

void ObjModel::addVertex(double v1, double v2, double v3){
	Vertex vertex;
	vertex.v1 = v1;
	vertex.v2 = v2;
	vertex.v3 = v3;
	m_vertices.push_back(vertex);
}

void ObjModel::addFace(int f1, int f2, int f3){
	Face face;
	face.f1 = f1;
	face.f2 = f2;
	face.f3 = f3;
	m_faces.push_back(face);
}

Vertex ObjModel::getVertex(int ind){
	return m_vertices[ind];
}

Face ObjModel::getFace(int ind){
	return m_faces[ind];
}

void ObjModel::printObjModelInfo(void){
	int size = m_vertices.size();
	cout << name + ":\n" << endl;
	for(int i = 0; i < size; i++){
		cout << "v " + to_string(m_vertices[i].v1) + " " + to_string(m_vertices[i].v2) + " " + to_string(m_vertices[i].v3) << endl;
	}

	size = m_faces.size();

	for(int i = 0; i < size; i++){
		cout << "f " + to_string(m_faces[i].f1) + " " + to_string(m_faces[i].f2) + " " + to_string(m_faces[i].f3) << endl;
	}
	cout << "\n" << endl;
}