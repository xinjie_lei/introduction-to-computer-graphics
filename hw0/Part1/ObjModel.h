#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <vector>
#include <iostream>

typedef struct _Vertex_ {
	double v1;
	double v2;
	double v3;
} Vertex;

typedef struct _Face_{
	int f1;
	int f2;
	int f3;
} Face;

class ObjModel{
	std::string name;
	std::vector<Vertex> m_vertices;
	std::vector<Face> m_faces;

public:
	ObjModel();
	~ObjModel();
	void setName(const std::string s);
	std::string getName(void);
	void addVertex(double v1, double v2, double v3);
	void addFace(int f1, int f2, int f3);
	Vertex getVertex(int ind);
	Face getFace(int ind);
	void printObjModelInfo(void);
};



#endif