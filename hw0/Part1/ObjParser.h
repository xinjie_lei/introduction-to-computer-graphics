#ifndef OBJPARSER_H
#define OBJPARSER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

#include "ObjModel.h"


class ObjParser{
	std::vector<ObjModel> m_models;

public:
	ObjParser();
	~ObjParser();
	int parse(char** filename);
	std::vector<std::string> split(const std::string &s, char delim);
	void printAll(void);
};

#endif