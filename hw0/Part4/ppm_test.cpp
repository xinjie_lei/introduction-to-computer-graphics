#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

using namespace std;

int main(int argc, char** argv){
	if(argc < 3){
		cerr << "usage: ./my_prog xres yres." << endl;
		return -1; 
	}
	// open a file
	ofstream outf("image.ppm");
	if(!outf){
		cerr << "couldn't open file image.ppm." << endl;
		return -1;
	}

	int xres = stoi(argv[1]);
	int yres = stoi(argv[2]);
	int r = xres > yres ? yres / 2 : xres / 2;
	double cx = xres / 2;
	double cy = yres / 2;

	string strx = string(argv[1]);
	string stry = string(argv[2]);

	outf << "P3" << endl;
	outf << strx + " " + stry << endl;
	outf << to_string(255) << endl;

	for(int row = 0; row < yres; row++){
		for(int col = 0; col < xres; col++){
			if((col - cx) * (col - cx) + (row - cy) * (row - cy) < r *r){
				outf << "0 255 255" << endl;
			}
			else{
				outf << "255 255 255" << endl;
			}
		}
	}

	return 0;
}