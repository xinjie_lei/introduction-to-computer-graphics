#ifndef OBJPARSER_H
#define OBJPARSER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

#include "ObjModel.h"


class ObjLoader{
	std::vector<ObjModel> m_initialmodels;
	std::vector<ObjModel> m_transformedmodels;
	
public:
	ObjLoader();
	~ObjLoader();
	int parse(char* filename);
	ObjModel* search(const std::string &s);
	int parseObjFile(const std::string modelname, const std::string &filename);
	std::vector<std::string> split(const std::string &s, char delim);
	void printTransformResults(void);
	void printAllModelName(void);
};

#endif