#include "ObjLoader.h"

using namespace std;

int main(int argc, char ** argv){
	
	if(argc < 2){
		cout << "usage: ./my_prog file1.obj file2.obj ...\n" << endl;
		return -1;
	}

	ObjLoader loader;
	loader.parse(argv[1]);
	loader.printTransformResults();
	return 0;
}