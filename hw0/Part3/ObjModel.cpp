#include "ObjModel.h"

using namespace Eigen;
using namespace std;

ObjModel::ObjModel(){

}
ObjModel::~ObjModel(){

}
ObjModel::ObjModel(const ObjModel* source){
	name = source -> name;

	int size = (source -> m_vertices).size();
	for(int i = 0; i < size; i++){
		m_vertices.push_back(Vertex((source -> m_vertices[i]).v1, (source -> m_vertices[i]).v2, (source -> m_vertices[i]).v3));
	}

	size = (source -> m_faces).size();
	for(int i = 0; i < size; i++){
		m_faces.push_back(Face((source -> m_faces[i]).f1, (source -> m_faces[i]).f2, (source -> m_faces[i]).f3));
	}

}

void ObjModel::setName(const string s){
	name = s;
}

string ObjModel::getName(){
	return name;
}

void ObjModel::addVertex(double v1, double v2, double v3){
	Vertex vertex = Vertex(v1, v2, v3);
	m_vertices.push_back(vertex);
}

Vertex ObjModel::getVertex(int ind){
	return m_vertices[ind];
}

void ObjModel::addFace(int f1, int f2, int f3){
	Face face = Face(f1, f2, f3);
	m_faces.push_back(face);
}

Face ObjModel::getFace(int ind){
	return m_faces[ind];
}

void ObjModel::setTranslation(double x, double y, double z){
	MatrixXd m(4, 4);
	m << 1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1;
	m_matrices.push_back(m);
}
void ObjModel::setRotation(double x, double y, double z, double theta){
	double length = sqrt(x*x + y*y + z*z);
	double ux = x / length;
	double uy = y / length;
	double uz = z / length;

	MatrixXd m(4, 4);
	m << ux * ux + (1 - ux * ux) * cos(theta), ux * uy * (1 - cos(theta)) - uz * sin(theta), ux * uz * (1 - cos(theta)) + uy * sin(theta), 0, \
	     uy * ux * (1 - cos(theta)) + uz * sin(theta), uy * uy + (1 - uy * uy) * cos(theta), uy * uz * (1 - cos(theta)) - ux * sin(theta), 0, \
	     uz * ux * (1 - cos(theta)) - uy * sin(theta), uz * uy * (1 - cos(theta)) + ux * sin(theta), uz * uz + (1 - uz * uz) * cos(theta), 0, \
	     0, 0, 0, 1;
	m_matrices.push_back(m);
}

void ObjModel::setScale(double x, double y, double z){
	MatrixXd m(4, 4);
	m << x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1;
	m_matrices.push_back(m);
}


void ObjModel::computeProduct(void){
	int size = m_matrices.size();
	transmatrix = m_matrices[size - 1];

	for(int i = size - 2; i > -1; i--){
		transmatrix = transmatrix * m_matrices[i];
	}
}

MatrixXd ObjModel::getProduct(void){
	return transmatrix;
}

MatrixXd ObjModel::getInverse(void){
	return transmatrix.inverse();

}

void ObjModel::printTransformToVertices(void){
	int size = m_vertices.size();
	for(int i = 0; i < size; i++){
		MatrixXd m(4, 1);
		m << m_vertices[i].v1, m_vertices[i].v2, m_vertices[i].v3, 1;
		MatrixXd mat = (transmatrix * m).transpose();
		m_vertices[i].v1 = mat(0);
		m_vertices[i].v2 = mat(1);
 		m_vertices[i].v3 = mat(2);
		cout << mat.block<1, 3>(0, 0) << endl;
	}
}
void ObjModel::printObjModelInfo(void){
	int size = m_vertices.size();
	for(int i = 0; i < size; i++){
		cout << "v " + to_string(m_vertices[i].v1) + " " + to_string(m_vertices[i].v2) + " " + to_string(m_vertices[i].v3) << endl;
	}

	size = m_faces.size();

	for(int i = 0; i < size; i++){
		cout << "f " + to_string(m_faces[i].f1) + " " + to_string(m_faces[i].f2) + " " + to_string(m_faces[i].f3) << endl;
	}

	size = m_matrices.size();
	for(int i = 0; i < size; i++){
		cout << m_matrices[i] << endl;
	}

	cout << transmatrix << endl;
}