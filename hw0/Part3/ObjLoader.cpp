#include "ObjLoader.h"

using namespace std;

ObjLoader::ObjLoader(){

}

ObjLoader::~ObjLoader(){

}

int ObjLoader::parse(char * filename){
	
	if(filename){	
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return -1;
		}

		ObjModel* initialmodel;
		ObjModel transformedmodel;
		vector<string> elems;
		string str;

		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			int size = elems.size();
			if(size == 2){
				parseObjFile(elems[0], elems[1]);
			}
			if(size == 1){
				if(!(initialmodel = search(elems[0]))){
					return -1;
				}
				transformedmodel = ObjModel(initialmodel);
			}
			if(size == 4 || size == 5){
				if(elems[0] == "t"){
					transformedmodel.setTranslation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
				if(elems[0] == "r"){
					transformedmodel.setRotation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL), stod(elems[4], NULL));
				}
				if(elems[0] == "s"){
					transformedmodel.setScale(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
			}
			if(size == 0 && transformedmodel.getName() != ""){
				m_transformedmodels.push_back(transformedmodel);
				transformedmodel.setName("");
			}
		}
	}
	return 0;
}

int ObjLoader::parseObjFile(const string modelname, const string &filename){
	if(!filename.empty()){
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return -1;
		}

		ObjModel model;
		model.setName(modelname);

		vector<string> elems;
		string str;
		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			if(elems.size() == 4){
				if(elems[0] == "v"){
					model.addVertex(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				} else{
					model.addFace(stoi(elems[1], NULL), stoi(elems[2], NULL), stoi(elems[3], NULL));
				}
			}
		}
		m_initialmodels.push_back(model);
	}

	return 0;
}

ObjModel* ObjLoader::search(const string &s){
	int size = m_initialmodels.size();
	for(int i = 0; i < size; i++){
		if(s.compare(m_initialmodels[i].getName()) == 0){
			return &(m_initialmodels[i]);
		}
	}
	return NULL;
}

vector<string> ObjLoader::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}

void ObjLoader::printTransformResults(void){
	int size = m_transformedmodels.size();
	int copycnt = 1;
	string lastname;
	for(int i = 0; i < size; i++){
		string name = m_transformedmodels[i].getName();
		if(lastname != name){
			copycnt = 1;
			lastname = name;
		}else{
			copycnt++;
		}
		cout << name + "_copy" + to_string(copycnt) << endl;
		m_transformedmodels[i].computeProduct();
		m_transformedmodels[i].printTransformToVertices();
	}
}
