#ifndef ASSIGNMENT_HPP
#define ASSIGNMENT_HPP

#include <vector>
#include <math.h>
#include <float.h>
#include <stdio.h>

class Camera;
class Scene;

namespace Assignment {

    void drawIOTest();
    void drawIntersectTest(Camera *camera);
    void raytrace(Camera camera, Scene scene);
  
};

#endif
