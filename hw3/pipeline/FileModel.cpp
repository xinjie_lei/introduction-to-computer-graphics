#include "FileModel.h"

using namespace std;

FileModel::FileModel(){

}

FileModel::~FileModel(){

}

int FileModel::parse(char * filename){
	if(filename){	
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return -1;
		}

		vector<string> elems;
		vector<string> light;
		string str;
		/*********************************************************** Camera ********************************************************/
		getline(inf, str);
		if(str != "camera:"){
			return -1;
		}

		// get postion
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "position" || elems.size() != 4){
			return -1;
		}
		m_cpos = CPosition(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));

		// get m_cori
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "orientation" || elems.size() != 5){
			return -1;
		}
		m_cori = COrientation(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL), stof(elems[4], NULL));

		// get perspective parameter
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "near" || elems.size() != 2){
			return -1;
		}
		m_ppara.near = stof(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "far" || elems.size() != 2){
			return -1;
		}
		m_ppara.far = stof(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "left" || elems.size() != 2){
			return -1;
		}
		m_ppara.left = stof(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "right" || elems.size() != 2){
			return -1;
		}
		m_ppara.right = stof(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "top" || elems.size() != 2){
			return -1;
		}
		m_ppara.top = stof(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "bottom" || elems.size() != 2){
			return -1;
		}
		m_ppara.bottom = stof(elems[1], NULL);

		/************************************************************** Light *********************************************************************/
		getline(inf, str);
		getline(inf, str);
		while(str != ""){
			Light l;
			elems = split(str, ',');
			light = split(elems[0], ' ');
			l.coord[0] = stof(light[1], NULL);
			l.coord[1] = stof(light[2], NULL);
			l.coord[2] = stof(light[3], NULL);
			l.coord[3] = 1;
			light = split(elems[1], ' ');
			l.rgb[0] = stof(light[1], NULL);
			l.rgb[1] = stof(light[2], NULL);
			l.rgb[2] = stof(light[3], NULL);
			l.kattn = stof(elems[2], NULL);
			m_lights.push_back(l);
			getline(inf, str);
		}

		/************************************************************* Object *********************************************************************/
		getline(inf, str);
		if(str != "objects:"){
			return -1;
		}

		vector<ObjModel> initialmodels;
		ObjModel* initialmodel;
		ObjModel transformedmodel;

		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			int size = elems.size();
			if(size == 2){
				if(elems[0] == "shininess"){
					transformedmodel.setShine(stof(elems[1], NULL));
				}else{
					parseObjFile(initialmodels, elems[0], elems[1]);
				}
			}
			if(size == 1){
				if(!(initialmodel = search(initialmodels, elems[0]))){
					return -1;
				}
				transformedmodel = ObjModel(initialmodel);
			}
			if(size == 4 || size == 5){
				if(elems[0] == "ambient"){
					transformedmodel.setAmbient(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));
				}
				if(elems[0] == "diffuse"){
					transformedmodel.setDiffuse(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));
				}
				if(elems[0] == "specular"){
					transformedmodel.setSpecular(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));
				}
				if(elems[0] == "t"){
					transformedmodel.setTranslation(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));
				}
				if(elems[0] == "r"){
					transformedmodel.setRotation(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL), stof(elems[4], NULL));
				}
				if(elems[0] == "s"){
					transformedmodel.setScale(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL));
				}
			}
			if(size == 0 && transformedmodel.getName() != ""){
				m_copies.push_back(transformedmodel);
				transformedmodel.setName("");
			}
		}
		return 0;
	}
	return -1;
}
int FileModel::parseObjFile(vector<ObjModel> &initialmodels, const string modelname, const string &filename){
	if(!filename.empty()){
		ifstream inf(filename);
		if(!inf){
			cerr << "parseObjFile can't open file." << filename << endl;
			return -1;
		}

		ObjModel model;
		vector<Triple> vertex_buffer;
		vector<Triple> normal_buffer;
		vector<string> elems;
		string str;
		model.setName(modelname);

		vertex_buffer.push_back(Triple(0,0,0));
		normal_buffer.push_back(Triple(0,0,0));
		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			if(elems.size() == 4){
				if(elems[0] == "v"){
					vertex_buffer.push_back(Triple(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL)));
				} else if(elems[0] == "vn"){
					normal_buffer.push_back(Triple(stof(elems[1], NULL), stof(elems[2], NULL), stof(elems[3], NULL)));
				} else{
					parseFace(elems[1], elems[2], elems[3], vertex_buffer, normal_buffer, model);
				}
			}
		}
		initialmodels.push_back(model);
	}

	return 0;
}

void FileModel::parseFace(string &s1, string &s2, string &s3, vector<Triple> &vertex_buffer, vector<Triple> &normal_buffer, ObjModel &model){
	vector<string> elems;
	elems = split(s1, '/');
	model.addVertex(vertex_buffer[stoi(elems[0], NULL)]);
	model.addNormal(normal_buffer[stoi(elems[2], NULL)]);

	elems = split(s2, '/');
	model.addVertex(vertex_buffer[stoi(elems[0], NULL)]);
	model.addNormal(normal_buffer[stoi(elems[2], NULL)]);

	elems = split(s3, '/');
	model.addVertex(vertex_buffer[stoi(elems[0], NULL)]);
	model.addNormal(normal_buffer[stoi(elems[2], NULL)]);

}

ObjModel* FileModel::search(vector<ObjModel> &initialmodels, const string &s){
	int size = initialmodels.size();
	for(int i = 0; i < size; i++){
		if(s.compare(initialmodels[i].getName()) == 0){
			return &(initialmodels[i]);
		}
	}
	return NULL;
}

vector<string> FileModel::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}
