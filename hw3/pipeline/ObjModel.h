#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <vector>
#include <iostream>
#include <cmath>

typedef struct _Triple_{
	float x;
	float y;
	float z;
	_Triple_(): x(0), y(0), z(0) {}
	_Triple_(float x, float y, float z): x(x), y(y), z(z) {}
	~_Triple_() {}
} Triple;

typedef struct _Transform_{
	float x;
	float y;
	float z;
	float angle;
	char type;
	_Transform_(): x(0), y(0), z(0), angle(0), type('N') {}
	_Transform_(float x, float y, float z, float angle, char type): x(x), y(y), z(z), angle(angle), type(type) {}
	~_Transform_() {}
} Transform;

class ObjModel{
	friend class Renderer;

	std::string name;
	std::vector<Triple> m_vertices;
	std::vector<Triple> m_normals;
	std::vector<Transform> m_matrices;

	float amb[3];
	float diff[3];
	float spec[3];
	float shine;

public:
	ObjModel();
	~ObjModel();
	ObjModel(const ObjModel* source);

	void setName(const std::string s);
	std::string getName(void);

	void addVertex(Triple &t);
	void addNormal(Triple &n);	
	void setTranslation(float x, float y, float z);
	void setRotation(float x, float y, float z, float theta);
	void setScale(float x, float y, float z);
	void setAmbient(float x, float y, float z);
	void setDiffuse(float x, float y, float z);
	void setSpecular(float x, float y, float z);
	void setShine(float x);

	void printObjModelInfo(void);
	std::vector<std::string> split(const std::string &s, char delim);

};



#endif