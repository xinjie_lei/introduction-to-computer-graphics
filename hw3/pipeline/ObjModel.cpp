#include "ObjModel.h"

using namespace std;

ObjModel::ObjModel(){

}
ObjModel::~ObjModel(){

}

ObjModel::ObjModel(const ObjModel* source){
	name = source -> name;

	int size = (source -> m_vertices).size();
	for(int i = 0; i < size; i++){
		m_vertices.push_back(source -> m_vertices[i]);
	}

	size = (source -> m_normals).size();
	for(int i = 0; i < size; i++){
		m_normals.push_back(source -> m_normals[i]);
	}

}

void ObjModel::setName(const string s){
	name = s;
}

string ObjModel::getName(){
	return name;
}

void ObjModel::addVertex(Triple &t){
	m_vertices.push_back(t);
}

void ObjModel::addNormal(Triple &n){
	m_normals.push_back(n);
}


void ObjModel::setTranslation(float x, float y, float z){
	Transform m = Transform(x, y, z, 0, 't');
	m_matrices.push_back(m);
}
void ObjModel::setRotation(float x, float y, float z, float theta){
	Transform m = Transform(x, y, z, theta, 'r');
	m_matrices.push_back(m);
}

void ObjModel::setScale(float x, float y, float z){
	Transform m = Transform(x, y, z, 0, 's');
	m_matrices.push_back(m);
}

void ObjModel::setAmbient(float x, float y, float z){
	amb[0] = x;
	amb[1] = y;
	amb[2] = z;
}
void ObjModel::setDiffuse(float x, float y, float z){
	diff[0] = x;
	diff[1] = y;
	diff[2] = z;
}

void ObjModel::setSpecular(float x, float y, float z){
	spec[0] = x;
	spec[1] = y;
	spec[2] = z;
}

void ObjModel::setShine(float x){
	shine = x;
}


void ObjModel::printObjModelInfo(void){
	int	size = m_vertices.size();
	for(int i = 0; i < size; i++){
		cout << "v " + to_string(m_vertices[i].x) + " " + to_string(m_vertices[i].y) + " " + to_string(m_vertices[i].z) << endl;
	}

	size = m_normals.size();
	for(int i = 0; i < size; i++){
		cout << "vn " + to_string(m_normals[i].x) + " " + to_string(m_normals[i].y) + " " + to_string(m_normals[i].z) << endl;
	}
	size =  m_matrices.size();
	for(int i = 0; i < size; i++){
 		cout << "transform " + to_string(m_matrices[i].x) + " " + to_string(m_matrices[i].y) + " " + to_string(m_matrices[i].z) \
 		        + " " + to_string(m_matrices[i].angle) + " " + to_string(m_matrices[i].type) << endl;
 	}
	cout << "amb: " + to_string(amb[0]) + " " + to_string(amb[1]) + " " + to_string(amb[2]) << endl;
	cout << "diff: " + to_string(diff[0]) + " " + to_string(diff[1]) + " " + to_string(diff[2]) << endl;
	cout << "spec: " + to_string(spec[0]) + " " + to_string(spec[1]) + " " + to_string(spec[2]) << endl;
	cout << "shineness: " + to_string(shine) << endl;

}
