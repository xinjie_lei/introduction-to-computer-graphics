#include "Renderer.h"

using namespace std;
using namespace Eigen;

Renderer::Renderer(){

}
Renderer::~Renderer(){

}

void Renderer::setsize(int xres, int yres){
	this -> xres = xres;
	this -> yres = yres;
}

int Renderer::parse(char * filename){
	m_model = FileModel();
	if(m_model.parse(filename) !=0){
		cerr << "can't parse file." << endl;
		return -1;
	}
	return 0;
}


void Renderer::init(void){

	// enable Gouraud shading 
	glShadeModel(GL_SMOOTH);

	// enable Backface culling
	glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    // enable Depth buffering
    glEnable(GL_DEPTH_TEST);

    // enable automatic normalization
    glEnable(GL_NORMALIZE);

    // enable vertex and normal array (pointer)
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);

    // set main matrix to projection matrix (for convertng camera to ndc)
    glMatrixMode(GL_PROJECTION);

    // set projection matrix based on frustum in world space
    glLoadIdentity();
    glFrustum(m_model.m_ppara.left, m_model.m_ppara.right,
              m_model.m_ppara.bottom, m_model.m_ppara.top,
              m_model.m_ppara.near, m_model.m_ppara.far);

    // set main matrix to modelview matrix (for converting world to camera)
    glMatrixMode(GL_MODELVIEW);

    // initialize light models
    init_lights();

    // initilize rotation quaternions
    lastrot << 1, 0, 0, 0;
    currrot << 1, 0, 0, 0;
}

void Renderer::init_lights(void){

	// Enable lighting model
	glEnable(GL_LIGHTING);

	int size = m_model.m_lights.size();

	for(int i = 0; i < size; i++){

		// set light id for light[i]
		int light_id = GL_LIGHT0 + i;
		glEnable(light_id);

		// bind ambient / diffuse / specular to one light color
		glLightfv(light_id, GL_AMBIENT, m_model.m_lights[i].rgb);
        glLightfv(light_id, GL_DIFFUSE, m_model.m_lights[i].rgb);
        glLightfv(light_id, GL_SPECULAR, m_model.m_lights[i].rgb);

        // set attenuation
        glLightf(light_id, GL_QUADRATIC_ATTENUATION, m_model.m_lights[i].kattn);
	}
}

void Renderer::display(void){

	// clear pixel grid and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// initialize ModelView matrix to identity
	glLoadIdentity();

	// set inverse camera transformation matrix
	glRotatef(-m_model.m_cori.angle * 180 / M_PI,
              m_model.m_cori.x, m_model.m_cori.y, m_model.m_cori.z);
	glTranslatef(-m_model.m_cpos.cx, -m_model.m_cpos.cy, -m_model.m_cpos.cz);
	
	// set up all lights in camera space;
	set_lights();

	// deal with mouse rotation
	glMultMatrixd((get_curr_rot()).data());
	
	// draw all objects on canvas
	draw_objects();

	// swap active and off-screen buffers
	glutSwapBuffers();
}

void Renderer::set_lights(){
    int size = m_model.m_lights.size();
    
    // set each lights position
    for(int i = 0; i < size; ++i) {
        int light_id = GL_LIGHT0 + i;
        glLightfv(light_id, GL_POSITION, m_model.m_lights[i].coord);
    }
}

void Renderer::draw_objects(){
	int size = m_model.m_copies.size();
    
    for(int i = 0; i < size; i++)
    {
    	// push current modelview matrix to stack
    	glPushMatrix();

    	// set model to world transform matrix
    	int num_trans = m_model.m_copies[i].m_matrices.size();
    	for(int j = num_trans - 1; j > -1; j--){
    		if(m_model.m_copies[i].m_matrices[j].type == 't'){
    			glTranslatef(m_model.m_copies[i].m_matrices[j].x,
                             m_model.m_copies[i].m_matrices[j].y,
                             m_model.m_copies[i].m_matrices[j].z);
    		}
    		else if(m_model.m_copies[i].m_matrices[j].type == 'r'){
    			glRotatef(m_model.m_copies[i].m_matrices[j].angle * 180 / M_PI,
                          m_model.m_copies[i].m_matrices[j].x,
                          m_model.m_copies[i].m_matrices[j].y,
                          m_model.m_copies[i].m_matrices[j].z);
    		}else{
    			glScalef(m_model.m_copies[i].m_matrices[j].x,
                         m_model.m_copies[i].m_matrices[j].y,
                         m_model.m_copies[i].m_matrices[j].z);
    		}
    	}

    	// set object physical properties
    	glMaterialfv(GL_FRONT, GL_AMBIENT, m_model.m_copies[i].amb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, m_model.m_copies[i].diff);
        glMaterialfv(GL_FRONT, GL_SPECULAR, m_model.m_copies[i].spec);
        glMaterialf(GL_FRONT, GL_SHININESS, m_model.m_copies[i].shine);

        // set vertex buffer
        glVertexPointer(3, GL_FLOAT, 0, &m_model.m_copies[i].m_vertices[0]);
        // set normal buffer
        glNormalPointer(GL_FLOAT, 0, &m_model.m_copies[i].m_normals[0]);

        // render results in glDrawArrays
        int buffer_size = m_model.m_copies[i].m_vertices.size();
        glDrawArrays(GL_TRIANGLES, 0, buffer_size);

        // pop the original matrix
        glPopMatrix();
    }

}

void Renderer::mouse_pressed(int button, int state, int x, int y)
{
   // check if left button is pressed
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        // store current mouse position
		mouse_x = x;
		mouse_y = y;

        // set is_pressed to true
		is_pressed = true;
	} else if(button == GLUT_LEFT_BUTTON && state == GLUT_UP){
		lastrot << currrot(0) * lastrot(0) - currrot(1) * lastrot(1) - currrot(2) * lastrot(2) - currrot(3) * lastrot(3),
		currrot(0) * lastrot(1) + currrot(1) * lastrot(0) + currrot(2) * lastrot(3) - currrot(3) * lastrot(2),
		currrot(0) * lastrot(2) + currrot(2) * lastrot(0) - currrot(1) * lastrot(3) + currrot(3) * lastrot(1),
		currrot(0) * lastrot(3) + currrot(3) * lastrot(0) + currrot(1) * lastrot(2) - currrot(2) * lastrot(1);

		currrot << 1, 0, 0, 0;
       	// set is_released to false
		is_pressed = false;
	}

}
void Renderer::mouse_moved(int x, int y){
	if(is_pressed){
		currrot = quat(mouse_x, mouse_y, x, y);
		glutPostRedisplay();
	}
}

Vector4d Renderer::quat(int pstart, int pdest, int x, int y){
	Vector3d start = (get_vector(pstart, pdest)).normalized();
	Vector3d dest = (get_vector(x, y)).normalized();
	

	double theta = acos(min((double)1, start.dot(dest)));
	Vector3d rotaxis = (start.cross(dest)).normalized();

	Vector4d q;
	q << cos(theta / 2), rotaxis(0) * sin(theta / 2), rotaxis(1) * sin(theta / 2), rotaxis(2) * sin(theta / 2);
	return q;
}

Vector3d Renderer::get_vector(int x, int y){
	Vector3d p;
	p << 1.0 * x / xres * 2 - 1, -(1.0 * y / yres * 2 - 1), 0;

	double square = p(0) * p(0) + p(1) * p(1);
	if(square <= 1){
		p(2) = sqrt(1 - square);
	}

	return p;
}

Matrix4d Renderer::get_curr_rot(void){
	Matrix4d last;
	last << 1-2*lastrot(2)*lastrot(2)-2*lastrot(3)*lastrot(3), 2*(lastrot(1)*lastrot(2)-lastrot(3)*lastrot(0)), 2*(lastrot(1)*lastrot(3)+lastrot(2)*lastrot(0)), 0,
			2*(lastrot(1)*lastrot(2)+lastrot(3)*lastrot(0)), 1-2*lastrot(1)*lastrot(1)-2*lastrot(3)*lastrot(3), 2*(lastrot(2)*lastrot(3)-lastrot(1)*lastrot(0)), 0, 
			2*(lastrot(1)*lastrot(3)-lastrot(2)*lastrot(0)), 2*(lastrot(2)*lastrot(3)+lastrot(1)*lastrot(0)), 1-2*lastrot(1)*lastrot(1)-2*lastrot(2)*lastrot(2), 0, 
			0, 0, 0, 1;
	Matrix4d curr;
	curr << 1-2*currrot(2)*currrot(2)-2*currrot(3)*currrot(3), 2*(currrot(1)*currrot(2)-currrot(3)*currrot(0)), 2*(currrot(1)*currrot(3)+currrot(2)*currrot(0)), 0,
			2*(currrot(1)*currrot(2)+currrot(3)*currrot(0)), 1-2*currrot(1)*currrot(1)-2*currrot(3)*currrot(3), 2*(currrot(2)*currrot(3)-currrot(1)*currrot(0)), 0, 
			2*(currrot(1)*currrot(3)-currrot(2)*currrot(0)), 2*(currrot(2)*currrot(3)+currrot(1)*currrot(0)), 1-2*currrot(1)*currrot(1)-2*currrot(2)*currrot(2), 0, 
			0, 0, 0, 1;
	return curr * last;
}

void Renderer::reshape(int width, int height){

    width = (width == 0) ? 1 : width;
    height = (height == 0) ? 1 : height;

    // enable NDC to scene conversion with xres and yres
    glViewport(0, 0, width, height);

    mouse_scale_x = (float) (m_model.m_ppara.right- m_model.m_ppara.left) / (float) width;
    mouse_scale_y = (float) (m_model.m_ppara.top - m_model.m_ppara.bottom) / (float) height;

    // enable redisplay if size changed
    glutPostRedisplay();
}
