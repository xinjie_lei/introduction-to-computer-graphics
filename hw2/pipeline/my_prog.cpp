#include "Renderer.h"

using namespace std;

int main(int argc, char ** argv){
	
	if(argc < 5){
		cout << "usage: ./wireframe file.txt xres yres mode\n" << endl;
		return -1;
	}

	Renderer renderer;
	renderer.setImageSize(argv[2], argv[3]);
	renderer.parse(argv[1]);
	renderer.generatePPM(argv[4]);
	return 0;
}