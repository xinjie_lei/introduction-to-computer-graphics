#include "Renderer.h"

using namespace std;
using namespace Eigen;

Renderer::Renderer(){

}
Renderer::~Renderer(){

}
void Renderer::setImageSize(char* xres, char* yres){
	this -> xres = stoi(xres);
	this -> yres = stoi(yres);
	m_image.resize(this -> yres, vector<vector<int>>(this -> xres, vector<int>(3)));
	buffer.resize(this -> yres, vector<double>(this -> xres, DBL_MAX));
}

int Renderer::parse(char * filename){
	m_model = FileModel();
	if(m_model.parse(filename) !=0){
		cerr << "can't parse file." << endl;
		return -1;
	}
	return 0;
}


void Renderer::setPerspectiveMatrix(void){
	m_ppMat = MatrixXd(4, 4);
	m_ppMat << 2 * m_model.m_ppara.near / (m_model.m_ppara.right - m_model.m_ppara.left), 0, (m_model.m_ppara.right + m_model.m_ppara.left) / (m_model.m_ppara.right - m_model.m_ppara.left), 0, \
			 0, 2 * m_model.m_ppara.near / (m_model.m_ppara.top - m_model.m_ppara.bottom), (m_model.m_ppara.top + m_model.m_ppara.bottom) / (m_model.m_ppara.top - m_model.m_ppara.bottom), 0, \
			 0, 0, -(m_model.m_ppara.far + m_model.m_ppara.near) / (m_model.m_ppara.far - m_model.m_ppara.near),  -2 * m_model.m_ppara.far * m_model.m_ppara.near / (m_model.m_ppara.far - m_model.m_ppara.near), \
			 0, 0, -1, 0;
}

void Renderer::setTransfromToNDCMatrix(void){
	m_wtNDCMat = m_ppMat * m_wtcMat;
}

void Renderer::transformWorldToNDC(void){
	int size = m_model.m_copies.size();
	for(int i = 0; i < size; i++){
		m_model.m_copies[i].transformVerticesToNDC(m_wtNDCMat);
	}
}

void Renderer::transformNDCtoScene(int xres, int yres){
	int size = m_model.m_copies.size();
	for(int i = 0; i < size; i++){
		m_model.m_copies[i].transformVerticesToScene(xres, yres);
	}

}

void Renderer::gouraudShading(void){
	int size = m_model.m_copies.size();
	int f1 = 0, f2 = 0, f3 = 0, n1 = 0, n2 = 0, n3 = 0;
	Vector3d ca, cb, cc;
	for(int i = 0; i < size; i++){
		int facesize = m_model.m_copies[i].m_faces.size();
		for(int j = 1; j < facesize; j++){
			f1 = m_model.m_copies[i].m_faces[j].fn1[0];
			f2 = m_model.m_copies[i].m_faces[j].fn2[0];
			f3 = m_model.m_copies[i].m_faces[j].fn3[0];
			n1 = m_model.m_copies[i].m_faces[j].fn1[1];
			n2 = m_model.m_copies[i].m_faces[j].fn2[1];
			n3 = m_model.m_copies[i].m_faces[j].fn3[1];
			rasterTriangle(i, f1, f2, f3, n1, n2, n3, 0);
		}
	} 

}

void Renderer::PhongShading(void){
	int size = m_model.m_copies.size();
	int f1 = 0, f2 = 0, f3 = 0, n1 = 0, n2 = 0, n3 = 0;
	for(int i = 0; i < size; i++){
		int facesize = m_model.m_copies[i].m_faces.size();
		for(int j = 1; j < facesize; j++){
			f1 = m_model.m_copies[i].m_faces[j].fn1[0];
			f2 = m_model.m_copies[i].m_faces[j].fn2[0];
			f3 = m_model.m_copies[i].m_faces[j].fn3[0];
			n1 = m_model.m_copies[i].m_faces[j].fn1[1];
			n2 = m_model.m_copies[i].m_faces[j].fn2[1];
			n3 = m_model.m_copies[i].m_faces[j].fn3[1];
			rasterTriangle(i, f1, f2, f3, n1, n2, n3, 1);
		}
	} 
}

void Renderer::rasterTriangle(int ind, int f1, int f2, int f3, int n1, int n2, int n3, int mode){
	
	Vector3d &v1ndc = m_model.m_copies[ind].m_ndcvert[f1];
	Vector3d &v2ndc = m_model.m_copies[ind].m_ndcvert[f2];
	Vector3d &v3ndc = m_model.m_copies[ind].m_ndcvert[f3];

	Vector3d cross = (v3ndc - v2ndc).cross(v1ndc - v2ndc);
	if(cross(2) < 0){return;}

	Vector3d &v1world = m_model.m_copies[ind].m_worldvert[f1];
	Vector3d &v2world = m_model.m_copies[ind].m_worldvert[f2];
	Vector3d &v3world = m_model.m_copies[ind].m_worldvert[f3];
	Vector3d &n1world = m_model.m_copies[ind].m_normals[n1];
	Vector3d &n2world = m_model.m_copies[ind].m_normals[n2];
	Vector3d &n3world = m_model.m_copies[ind].m_normals[n3];
	Vector2d &v1scene = m_model.m_copies[ind].m_scenes[f1];
	Vector2d &v2scene = m_model.m_copies[ind].m_scenes[f2];
	Vector2d &v3scene = m_model.m_copies[ind].m_scenes[f3];

	double xmin = min(min(v1scene(0), v2scene(0)), v3scene(0));
	double xmax = max(max(v1scene(0), v2scene(0)), v3scene(0));
	double ymin = min(min(v1scene(1), v2scene(1)), v3scene(1));
	double ymax = max(max(v1scene(1), v2scene(1)), v3scene(1));

	for(int x = xmin; x <= xmax; x++){
		for(int y = ymin; y <= ymax; y++){
			Vector2d xy;
			xy << x, y;
			double alpha = computeCoeff(v1scene, v2scene, v3scene, xy);
			double beta = computeCoeff(v2scene, v3scene, v1scene, xy);
			double gamma = computeCoeff(v3scene, v1scene, v2scene, xy);
			if(alpha < 0 || alpha > 1 || beta < 0 || beta > 1 || gamma < 0 || gamma > 1){continue;}
			Vector3d ndc = alpha * v1ndc + beta * v2ndc + gamma * v3ndc;
			if(ndc(0) < -1 || ndc(0) > 1 || ndc(1) < -1 || ndc(1) > 1 || ndc(2) < -1 || ndc(2) > 1){continue;}
			if(ndc(2) > buffer[yres - y - 1][x]){continue;}
			buffer[yres - y - 1][x] = ndc(2);
			if(mode == 1){
				Vector3d n = alpha * n1world + beta * n2world + gamma * n3world;
				Vector3d v = alpha * v1world + beta * v2world + gamma * v3world;
				Vector3d color = lighting(ind, v, n);
				m_image[yres - y - 1][x] = {(int)round(color(0) * 255), (int)round(color(1) * 255), (int)round(color(2) * 255)};
			}else{
				Vector3d ca = lighting(ind, v1world, n1world);
				Vector3d cb = lighting(ind, v2world, n2world);
				Vector3d cc = lighting(ind, v3world, n3world);
				int r = round((alpha * ca(0) + beta * cb(0) + gamma * cc(0)) * 255);
				int g = round((alpha * ca(1) + beta * cb(1) + gamma * cc(1)) * 255);
				int b = round((alpha * ca(2) + beta * cb(2) + gamma * cc(2)) * 255);
				m_image[yres - y - 1][x] = {r, g, b};
			}
		}
	}
}

double Renderer::computeCoeff(Vector2d &a, Vector2d &b, Vector2d &c, Vector2d &x){
	Matrix2d t(2, 2);
	t << b(0) - a(0), c(0) - a(0), b(1) - a(1), c(1) - a(1);
	Matrix2d sub(2, 2);
	sub << b(0) - x(0), c(0) - x(0), b(1) - x(1), c(1) - x(1);
	double area = t.determinant();
	if(area == 0){return -1;}
	return sub.determinant() / area;
}

Vector3d Renderer::lighting(int ind, Vector3d &v, Vector3d &n){
	Vector3d diffuse_sum = {0, 0, 0};
	Vector3d specular_sum = {0, 0, 0};

	Vector3d e;
	e << m_model.m_cpos.cx, m_model.m_cpos.cy, m_model.m_cpos.cz;

	Vector3d edir = (e - v).normalized();

	int size = m_model.m_lights.size();
	for(int i = 0; i < size; i++){
		Vector3d lp = Map<Vector3d>(m_model.m_lights[i].coord.data());
		Vector3d temp_lc = Map<Vector3d>(m_model.m_lights[i].rgb.data());
		Vector3d dist = lp - v;
		Vector3d lc = temp_lc / (1 + m_model.m_lights[i].kattn * dist.dot(dist));
		Vector3d ldir = dist.normalized();
		Vector3d ldiff = lc * max((double)0,  n.dot(ldir));
		diffuse_sum += ldiff;
		Vector3d lspec = lc * pow(max((double)0, n.dot((edir+ldir).normalized())), m_model.m_copies[ind].shine);
		specular_sum += lspec;
	}
	Vector3d clamp;
	clamp << 1, 1, 1;
	return clamp.cwiseMin(m_model.m_copies[ind].amb + diffuse_sum.cwiseProduct(m_model.m_copies[ind].diff) \
	                      + specular_sum.cwiseProduct(m_model.m_copies[ind].spec));
}

int Renderer::generatePPM(char * mode){
	setCameraToWorldMatrix();
	setWorldToCameraMatrix();
	setPerspectiveMatrix();
	setTransfromToNDCMatrix();
	transformWorldToNDC();
	transformNDCtoScene(xres, yres);
	if(*mode == '0'){
		gouraudShading();
	}else{
		PhongShading();
	}
	
	cout << "P3" << endl;
	cout << to_string(xres) + " " + to_string(yres) << endl;
	cout << to_string(255) << endl;

	int rows = m_image.size();
	int cols = m_image[0].size();

	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			cout << to_string(m_image[i][j][0]) + " " + to_string(m_image[i][j][1]) + " " + to_string(m_image[i][j][2]) << endl;
		}
	}
	return 0;

}
