#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <vector>
#include <iostream>
#include <cmath>
#include <Eigen/Dense>

typedef struct _Face_{
	std::vector<int> fn1;
	std::vector<int> fn2;
	std::vector<int> fn3;
	_Face_(): fn1({0, 0}), fn2({0, 0}), fn3({0, 0}) {}
	_Face_(std::vector<int> fn1, std::vector<int> fn2, std::vector<int> fn3): fn1({fn1[0], fn1[1]}), fn2({fn2[0], fn2[1]}), fn3({fn3[0], fn3[1]}) {}
	~_Face_() {} 
} Face;

class ObjModel{
	friend class Renderer;

	std::string name;
	std::vector<Eigen::Vector3d> m_vertices;
	std::vector<Eigen::Vector3d> m_worldvert;
	std::vector<Eigen::Vector3d> m_ndcvert;
	std::vector<Eigen::Vector2d> m_scenes;
	std::vector<Eigen::Vector3d> m_normals;
	std::vector<Face> m_faces;
	std::vector<Eigen::Matrix4d> m_matrices;
	std::vector<char> m_mattype;
	Eigen::Matrix4d transmatrix;
	Eigen::Matrix4d normaltrans;

	Eigen::Vector3d amb;
	Eigen::Vector3d diff;
	Eigen::Vector3d spec;
	double shine;

public:
	ObjModel();
	~ObjModel();
	ObjModel(const ObjModel* source);

	void setName(const std::string s);
	std::string getName(void);

	void addVertex(double v1, double v2, double v3);
	void addNormal(double n1, double n2, double n3);
	void addFace(std::string &s1, std::string &s2, std::string &s3);	
	void setTranslation(double x, double y, double z);
	void setRotation(double x, double y, double z, double theta);
	void setScale(double x, double y, double z);
	void setAmbient(double x, double y, double z);
	void setDiffuse(double x, double y, double z);
	void setSpecular(double x, double y, double z);
	void setShine(double x);
	void computeTransMat(void);
	void transformVerticesToWorld(void);
	void computeNormalTransMat(void);
	void transformNormalsToWorld(void);
	void transformVerticesToNDC(Eigen::MatrixXd mat);
	void transformVerticesToScene(int xres, int yres);

	void printObjModelInfo(void);
	std::vector<std::string> split(const std::string &s, char delim);

};



#endif