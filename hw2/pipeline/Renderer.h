#ifndef RENDERER_H
#define RENDERER_H

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <iostream>
#include <string>
#include <fstream>
#include <cfloat>
#include <cstdio>
#include <cmath>
#include "FileModel.h"

class Renderer{
	int xres;
	int yres;
	FileModel m_model;
	Eigen::Matrix4d m_wtcMat;
	Eigen::Matrix4d m_ctwMat;
	Eigen::Matrix4d m_ppMat;
	Eigen::Matrix4d m_wtNDCMat;
	std::vector<std::vector<std::vector<int>>> m_image;
	std::vector<std::vector<double>> buffer;

public:
	Renderer();
	~Renderer();

	void setImageSize(char* xres, char* yres);
	int parse(char * filename);
	void setWorldToCameraMatrix(void);
	void setCameraToWorldMatrix(void);
	void setPerspectiveMatrix(void);
	void setTransfromToNDCMatrix(void);
	void transformGeometry(void);
	void transformNormalGeometry(void);
	void transformWorldToNDC(void);
	void transformNDCtoScene(int xres, int yres);
	void gouraudShading(void);
	void PhongShading(void);
	void rasterTriangle(int ind, int f1, int f2, int f3, int n1, int n2, int n3, int mode);
	double computeCoeff(Eigen::Vector2d &a, Eigen::Vector2d &b, Eigen::Vector2d &c, Eigen::Vector2d &x);
	Eigen::Vector3d lighting(int ind, Eigen::Vector3d &vertice, Eigen::Vector3d &normal);
	int generatePPM(char * mode);
	void printVerticesinNDC(void);
};

#endif