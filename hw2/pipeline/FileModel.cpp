#include "FileModel.h"

using namespace std;

FileModel::FileModel(){

}

FileModel::~FileModel(){

}

int FileModel::parse(char * filename){
	if(filename){	
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return -1;
		}

		vector<string> elems;
		vector<string> light;
		string str;
		/*********************************************************** Camera ********************************************************/
		getline(inf, str);
		if(str != "camera:"){
			return -1;
		}

		// get postion
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "position" || elems.size() != 4){
			return -1;
		}
		m_cpos = CPosition(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));

		// get m_cori
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "orientation" || elems.size() != 5){
			return -1;
		}
		m_cori = COrientation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL), stod(elems[4], NULL));

		// get perspective parameter
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "near" || elems.size() != 2){
			return -1;
		}
		m_ppara.near = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "far" || elems.size() != 2){
			return -1;
		}
		m_ppara.far = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "left" || elems.size() != 2){
			return -1;
		}
		m_ppara.left = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "right" || elems.size() != 2){
			return -1;
		}
		m_ppara.right = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "top" || elems.size() != 2){
			return -1;
		}
		m_ppara.top = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "bottom" || elems.size() != 2){
			return -1;
		}
		m_ppara.bottom = stod(elems[1], NULL);

		/************************************************************** Light *********************************************************************/
		getline(inf, str);
		getline(inf, str);
		while(str != ""){
			Light l;
			elems = split(str, ',');
			light = split(elems[0], ' ');
			l.coord = {stod(light[1], NULL), stod(light[2], NULL), stod(light[3], NULL)};
			light = split(elems[1], ' ');
			l.rgb = {stod(light[1], NULL), stod(light[2], NULL), stod(light[3], NULL)};
			l.kattn = stod(elems[2], NULL);
			m_lights.push_back(l);
			getline(inf, str);
		}

		/************************************************************* Object *********************************************************************/
		getline(inf, str);
		if(str != "objects:"){
			return -1;
		}

		vector<ObjModel> initialmodels;
		ObjModel* initialmodel;
		ObjModel transformedmodel;

		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			int size = elems.size();
			if(size == 2){
				if(elems[0] == "shininess"){
					transformedmodel.setShine(stod(elems[1], NULL));
				}else{
					parseObjFile(initialmodels, elems[0], elems[1]);
				}
			}
			if(size == 1){
				if(!(initialmodel = search(initialmodels, elems[0]))){
					return -1;
				}
				transformedmodel = ObjModel(initialmodel);
			}
			if(size == 4 || size == 5){
				if(elems[0] == "ambient"){
					transformedmodel.setAmbient(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
				if(elems[0] == "diffuse"){
					transformedmodel.setDiffuse(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
				if(elems[0] == "specular"){
					transformedmodel.setSpecular(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
				if(elems[0] == "t"){
					transformedmodel.setTranslation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
				if(elems[0] == "r"){
					transformedmodel.setRotation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL), stod(elems[4], NULL));
				}
				if(elems[0] == "s"){
					transformedmodel.setScale(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
			}
			if(size == 0 && transformedmodel.getName() != ""){
				m_copies.push_back(transformedmodel);
				transformedmodel.setName("");
			}
		}

		int size = m_copies.size();
		for(int i = 0; i < size; i++){
			m_copies[i].computeTransMat();
			m_copies[i].transformVerticesToWorld();
			m_copies[i].computeNormalTransMat();
			m_copies[i].transformNormalsToWorld();
		}
		return 0;
	}
	return -1;
}
int FileModel::parseObjFile(vector<ObjModel> &initialmodels, const string modelname, const string &filename){
	if(!filename.empty()){
		ifstream inf(filename);
		if(!inf){
			cerr << "parseObjFile can't open file." << filename << endl;
			return -1;
		}

		ObjModel model;
		string init = "0//0";
		model.setName(modelname);
		model.addVertex(0, 0 ,0);
		model.addNormal(0, 0, 0);
		model.addFace(init, init, init);

		vector<string> elems;
		string str;
		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			if(elems.size() == 4){
				if(elems[0] == "v"){
					model.addVertex(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				} else if(elems[0] == "vn"){
					model.addNormal(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				} else{
					model.addFace(elems[1], elems[2], elems[3]);
				}
			}
		}
		initialmodels.push_back(model);
	}

	return 0;
}

ObjModel* FileModel::search(vector<ObjModel> &initialmodels, const string &s){
	int size = initialmodels.size();
	for(int i = 0; i < size; i++){
		if(s.compare(initialmodels[i].getName()) == 0){
			return &(initialmodels[i]);
		}
	}
	return NULL;
}

vector<string> FileModel::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}
