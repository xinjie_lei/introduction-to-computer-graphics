#include "ObjModel.h"

using namespace Eigen;
using namespace std;

ObjModel::ObjModel(){

}
ObjModel::~ObjModel(){

}

ObjModel::ObjModel(const ObjModel* source){
	name = source -> name;

	int size = (source -> m_vertices).size();
	for(int i = 0; i < size; i++){
		m_vertices.push_back(source -> m_vertices[i]);
	}

	for(int i = 0; i < size; i++){
		m_worldvert.push_back(source -> m_worldvert[i]);
	}

	for(int i = 0; i < size; i++){
		m_ndcvert.push_back(source -> m_ndcvert[i]);
	}

	size = (source -> m_normals).size();
	for(int i = 0; i < size; i++){
		m_normals.push_back(source -> m_normals[i]);
	}


	size = (source -> m_faces).size();
	for(int i = 0; i < size; i++){
		m_faces.push_back(Face((source -> m_faces[i]).fn1, (source -> m_faces[i]).fn2, (source -> m_faces[i]).fn3));
	}

}

void ObjModel::setName(const string s){
	name = s;
}

string ObjModel::getName(){
	return name;
}

void ObjModel::addVertex(double v1, double v2, double v3){
	Vector3d v;
	v << v1, v2, v3;
	m_vertices.push_back(v);
	m_worldvert.push_back(v);
	m_ndcvert.push_back(v);
}

void ObjModel::addNormal(double n1, double n2, double n3){
	Vector3d n;
	n << n1, n2, n3;
	m_normals.push_back(n);
}

void ObjModel::addFace(string &s1, string &s2, string &s3){
	Face face;
	vector<string> elems;
	elems = split(s1, '/');
	face.fn1 = {stoi(elems[0], NULL), stoi(elems[2], NULL)};

	elems = split(s2, '/');
	face.fn2 = {stoi(elems[0], NULL), stoi(elems[2], NULL)};

	elems = split(s3, '/');
	face.fn3 = {stoi(elems[0], NULL), stoi(elems[2], NULL)};

	m_faces.push_back(face);
}

void ObjModel::setTranslation(double x, double y, double z){
	MatrixXd m(4, 4);
	m << 1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1;
	m_matrices.push_back(m);
	m_mattype.push_back('t');
}
void ObjModel::setRotation(double x, double y, double z, double theta){
	double length = sqrt(x*x + y*y + z*z);
	double ux = x / length;
	double uy = y / length;
	double uz = z / length;

	MatrixXd m(4, 4);
	m << ux * ux + (1 - ux * ux) * cos(theta), ux * uy * (1 - cos(theta)) - uz * sin(theta), ux * uz * (1 - cos(theta)) + uy * sin(theta), 0, \
	     uy * ux * (1 - cos(theta)) + uz * sin(theta), uy * uy + (1 - uy * uy) * cos(theta), uy * uz * (1 - cos(theta)) - ux * sin(theta), 0, \
	     uz * ux * (1 - cos(theta)) - uy * sin(theta), uz * uy * (1 - cos(theta)) + ux * sin(theta), uz * uz + (1 - uz * uz) * cos(theta), 0, \
	     0, 0, 0, 1;
	m_matrices.push_back(m);
	m_mattype.push_back('r');
}

void ObjModel::setScale(double x, double y, double z){
	MatrixXd m(4, 4);
	m << x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1;
	m_matrices.push_back(m);
	m_mattype.push_back('s');
}
void ObjModel::setAmbient(double x, double y, double z){
	amb << x, y, z;
}
void ObjModel::setDiffuse(double x, double y, double z){
	diff << x, y, z;
}
void ObjModel::setSpecular(double x, double y, double z){
	spec << x, y, z;
}
void ObjModel::setShine(double x){
	shine = x;
}


void ObjModel::computeTransMat(void){
	int size = m_matrices.size();
	transmatrix = m_matrices[size - 1];

	for(int i = size - 2; i > -1; i--){
		transmatrix = transmatrix * m_matrices[i];
	}
}

void ObjModel::computeNormalTransMat(void){
	int size = m_matrices.size();
	int i = size - 1;
	for(; i > -1; i--){
		if(m_mattype[i] != 't'){
			normaltrans = m_matrices[i];
			break;
		}
	}
	i--;
	for(; i > -1; i--){
		if(m_mattype[i] != 't'){
			normaltrans = normaltrans * m_matrices[i];
		}
	}
	normaltrans = (normaltrans.inverse()).transpose();
}

void ObjModel::transformNormalsToWorld(void){
	int size = m_normals.size();
	Vector4d n;
	for(int i = 1; i < size; i++){
		n << m_normals[i], 1;
		Vector4d mat = (normaltrans * n).transpose();
		m_normals[i](0) = mat(0) / sqrt(mat(0) * mat(0) + mat(1) * mat(1) + mat(2) * mat(2));
		m_normals[i](1) = mat(1) / sqrt(mat(0) * mat(0) + mat(1) * mat(1) + mat(2) * mat(2));
 		m_normals[i](2) = mat(2) / sqrt(mat(0) * mat(0) + mat(1) * mat(1) + mat(2) * mat(2));
	}
}
void ObjModel::transformVerticesToWorld(void){
	int size = m_vertices.size();
	Vector4d v;
	for(int i = 1; i < size; i++){
		v << m_vertices[i], 1;
		Vector4d mat = (transmatrix * v).transpose();
		m_worldvert[i](0) = mat(0);
		m_worldvert[i](1) = mat(1);
		m_worldvert[i](2) = mat(2);
	}
}

void ObjModel::transformVerticesToNDC(MatrixXd mat){
	transmatrix = mat;
	Vector4d v;
	int size = m_vertices.size();
	for(int i = 1; i < size; i++){
		v << m_worldvert[i], 1;
		Vector4d mat = (transmatrix * v).transpose();
 		m_ndcvert[i](0) = mat(0) / mat(3);
		m_ndcvert[i](1) = mat(1) / mat(3);
 		m_ndcvert[i](2) = mat(2) / mat(3);
	}
}

void ObjModel::transformVerticesToScene(int xres, int yres){
	int size = m_vertices.size();
	Vector2d scene;
	scene << 0,0;
	m_scenes.push_back(scene);
	for(int i = 1; i < size; i++){
		scene << round(xres / 2 * (m_ndcvert[i](0) + 1)), round(yres / 2 * (m_ndcvert[i](1) + 1));
		m_scenes.push_back(scene);
	}
}

void ObjModel::printObjModelInfo(void){

	int size = m_normals.size();
	for(int i = 0; i < size; i++){
		cout << "vn " << m_normals[i] << endl;
	}

	size = m_faces.size();
	for(int i = 0; i < size; i++){
		cout << "f " + to_string(m_faces[i].fn1[0]) + "//" + to_string(m_faces[i].fn1[1]) \
		 		+ " " + to_string(m_faces[i].fn2[0]) + "//" + to_string(m_faces[i].fn2[1]) \
				+ " " + to_string(m_faces[i].fn3[0]) + "//" + to_string(m_faces[i].fn3[1]) << endl;
	}
	cout << "transmatrix:" << endl;
	cout << transmatrix << endl;
	cout << "normaltrans:" << endl;
	cout << normaltrans << endl;

	size = m_vertices.size();
	for(int i = 0; i < size; i++){
		cout << "worldv " << m_worldvert[i] << endl;
	}

	size = m_normals.size();
	for(int i = 0; i < size; i++){
		cout << "ndcv " << m_ndcvert[i] << endl;
	}

	size = m_normals.size();
	for(int i = 0; i < size; i++){
		cout << "scenev " << m_scenes[i] << endl;
	}


}

vector<string> ObjModel::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}
