#include "Renderer.h"

using namespace std;
using namespace Eigen;

Renderer::Renderer(){

}
Renderer::~Renderer(){

}
void Renderer::setImageSize(char* xres, char* yres){
	this -> xres = stoi(xres);
	this -> yres = stoi(yres);
}

int Renderer::parse(char * filename){
	m_model = FileModel();
	if(m_model.parse(filename) !=0){
		cerr << "can't parse file." << endl;
		return -1;
	}
	return 0;
}

void Renderer::setWorldToCameraMatrix(void){
	m_wtcMat = m_ctwMat.inverse();
}

void Renderer::setCameraToWorldMatrix(void){
	MatrixXd t(4, 4);
	t << 1, 0, 0, m_model.m_cpos.cx, 0, 1, 0, m_model.m_cpos.cy, 0, 0, 1, m_model.m_cpos.cz, 0, 0, 0, 1;

	double length = sqrt(m_model.m_cori.x * m_model.m_cori.x + m_model.m_cori.y * m_model.m_cori.y + m_model.m_cori.z * m_model.m_cori.z);
	double ux = m_model.m_cori.x / length;
	double uy = m_model.m_cori.y / length;
	double uz = m_model.m_cori.z / length;
	double theta = m_model.m_cori.angle;

	MatrixXd r(4, 4);
	r << ux * ux + (1 - ux * ux) * cos(theta), ux * uy * (1 - cos(theta)) - uz * sin(theta), ux * uz * (1 - cos(theta)) + uy * sin(theta), 0, \
	     uy * ux * (1 - cos(theta)) + uz * sin(theta), uy * uy + (1 - uy * uy) * cos(theta), uy * uz * (1 - cos(theta)) - ux * sin(theta), 0, \
	     uz * ux * (1 - cos(theta)) - uy * sin(theta), uz * uy * (1 - cos(theta)) + ux * sin(theta), uz * uz + (1 - uz * uz) * cos(theta), 0, \
	     0, 0, 0, 1;
	m_ctwMat = t * r;
}

void Renderer::setPerspectiveMatrix(void){
	m_ppMat = MatrixXd(4, 4);
	m_ppMat << 2 * m_model.m_ppara.near / (m_model.m_ppara.right - m_model.m_ppara.left), 0, (m_model.m_ppara.right + m_model.m_ppara.left) / (m_model.m_ppara.right - m_model.m_ppara.left), 0, \
			 0, 2 * m_model.m_ppara.near / (m_model.m_ppara.top - m_model.m_ppara.bottom), (m_model.m_ppara.top + m_model.m_ppara.bottom) / (m_model.m_ppara.top - m_model.m_ppara.bottom), 0, \
			 0, 0, -(m_model.m_ppara.far + m_model.m_ppara.near) / (m_model.m_ppara.far - m_model.m_ppara.near),  -2 * m_model.m_ppara.far * m_model.m_ppara.near / (m_model.m_ppara.far - m_model.m_ppara.near), \
			 0, 0, -1, 0;
}

void Renderer::setTransfromToNDCMatrix(void){
	m_wtNDCMat = m_ppMat * m_wtcMat;
}

void Renderer::transformGeometry(void){
	int size = m_model.m_transformedmodels.size();
	for(int i = 0; i < size; i++){
		m_model.m_transformedmodels[i].computeTransMat();
		m_model.m_transformedmodels[i].transformVerticesToWorld();
	}
}
void Renderer::transformWorldToNDC(void){
	int size = m_model.m_transformedmodels.size();
	for(int i = 0; i < size; i++){
		m_model.m_transformedmodels[i].transformVerticesToNDC(m_wtNDCMat);
	}
}

void Renderer::transformNDCtoScene(int xres, int yres){
	int size = m_model.m_transformedmodels.size();
	for(int i = 0; i < size; i++){
		m_model.m_transformedmodels[i].transformVerticesToScene(xres, yres);
	}

}
void Renderer::generateImage(void){
	m_image.resize(yres, vector<char>(xres));
	int size = m_model.m_transformedmodels.size();
	int x0 = 0, y0 = 0, x1 = 0, y1 = 0, f1 = 0, f2 = 0, f3 = 0;
	bool f1inside = true, f2inside = true, f3inside = true;

	for(int i = 0; i < size; i++){
		int facesize = m_model.m_transformedmodels[i].m_faces.size();
		for(int j = 0; j < facesize; j++){
			f1 = m_model.m_transformedmodels[i].m_faces[j].f1 - 1;
			f2 = m_model.m_transformedmodels[i].m_faces[j].f2 - 1;
			f3 = m_model.m_transformedmodels[i].m_faces[j].f3 - 1;
			f1inside = m_model.m_transformedmodels[i].m_vertices[f1].inside;
			f2inside = m_model.m_transformedmodels[i].m_vertices[f2].inside;
			f3inside = m_model.m_transformedmodels[i].m_vertices[f3].inside;

			if(f1inside && f2inside){
				x0 = m_model.m_transformedmodels[i].m_vertices[f1].sx;
				y0 = m_model.m_transformedmodels[i].m_vertices[f1].sy;
				x1 = m_model.m_transformedmodels[i].m_vertices[f2].sx;
				y1 = m_model.m_transformedmodels[i].m_vertices[f2].sy;
				line(x0, y0, x1, y1);
			}


			if(f2inside && f3inside){
				x0 = m_model.m_transformedmodels[i].m_vertices[f2].sx;
				y0 = m_model.m_transformedmodels[i].m_vertices[f2].sy;
				x1 = m_model.m_transformedmodels[i].m_vertices[f3].sx;
				y1 = m_model.m_transformedmodels[i].m_vertices[f3].sy;
				line(x0, y0, x1, y1);
			}

			if(f1inside && f3inside){
				x0 = m_model.m_transformedmodels[i].m_vertices[f1].sx;
				y0 = m_model.m_transformedmodels[i].m_vertices[f1].sy;
				x1 = m_model.m_transformedmodels[i].m_vertices[f3].sx;
				y1 = m_model.m_transformedmodels[i].m_vertices[f3].sy;
				line(x0, y0, x1, y1);
			}
		}
	} 

}
void Renderer::line(int x0, int y0, int x1, int y1){
	// following swapping is assumed that vectors in 8 octants are pointing outward, and octants are named counter-clockwise.
	// the 1st octant is the octant with known Bresenham algorithm.
	bool transposed = false;
	// draw lines in 2nd octants is the same as drawing lines in the 1st octants, but when setting pixel, setting the pixel in transposed coordinates (actual coordinates)
	// of the coordinates in the 1st octants.
	// same as 3rd and 8th, 7th and 4th, 6th and 5th, where the former is the octants to be transposed. 
	if(abs(x1 - x0) < abs(y1 - y0)){
		swap(x0, y0);
		swap(x1, y1);
		transposed = true;
	}

	// drawing line based on vector <x1 -x0, y1 - y0> (x1 > x0) is the same as drawing line based on <x0 - x1, y0 -y1>, because line doesn't have direction.
	// when drawing line based on vectors in 2nd, 3rd, 6th, 7th, the vectors are transposed to 1st, 8th, 5th, and 4th octants.
	// drawing line based on vectors in 5th is the same as drawing line on 1st octants with point1 and point2 swap location.
	// same story for drawing line based on vectors in 4th octants.

	if(x1 < x0){
		swap(x0, x1);
		swap(y0, y1);
	}

	int dx = x1 - x0;

	// from discussion above, we finally transform all line drawing in other octants to line drawing in 1st and 8th octants.
	// the diffence between 1st and 8th octants is dy. let dy = |y1 - y0|
	int dy = abs(y1 - y0);
	int y = y0;
	int error = 0;
	for(int x = x0; x <= x1; x++){
		// our screen coordinates is in a coordinate system like this:
		// 0 -----------------------------> xres
		// yres
		// |
		// |
		// 0
		// therefore, when setting pixel, be careful about setting in coordinates in the right position in real pixel grid coordinate system.
		if(transposed){
			m_image[xres - x - 1][y] = 1;
		}else{
			m_image[yres - y - 1][x] = 1;
		}
		if(2 * (error + dy) < dx){
			error += dy;
		}else{
			error += (dy - dx);
			// if y1 > y0, then we are drawing line in 1st octant, next y = current y + 1;
			// if y1 < y0, then we are drawing line in 8th octant, next y = current y - 1;
			y += (y1 > y0) ? 1 : -1;
		}
	}
}

int Renderer::generatePPM(){
	setCameraToWorldMatrix();
	setWorldToCameraMatrix();
	setPerspectiveMatrix();
	setTransfromToNDCMatrix();
    transformGeometry();
    int size = m_model.m_transformedmodels.size();
    for(int i = 0; i < size; i++){
    	m_model.m_transformedmodels[i].printObjModelInfo();
    }
	transformWorldToNDC();
	transformNDCtoScene(xres, yres);
	generateImage();

	// open a file
	ofstream outf("image.ppm");
	if(!outf){
		cerr << "couldn't open file image.ppm." << endl;
		return -1;
	}

	outf << "P3" << endl;
	outf << to_string(yres) + " " + to_string(xres) << endl;
	outf << to_string(255) << endl;

	int rows = m_image.size();
	int cols = m_image[0].size();

	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			if(m_image[i][j] == 1){
				outf << "255 255 255" << endl;
			}else{
				outf << "0 0 0" << endl;
			}
		}
	}
	return 0;

}