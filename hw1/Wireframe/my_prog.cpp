#include "Renderer.h"

using namespace std;

int main(int argc, char ** argv){
	
	if(argc < 4){
		cout << "usage: ./wireframe file.txt xres yres\n" << endl;
		return -1;
	}

	Renderer renderer;
	renderer.setImageSize(argv[2], argv[3]);
	renderer.parse(argv[1]);
	renderer.generatePPM();
	
	return 0;
}