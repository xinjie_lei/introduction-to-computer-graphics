#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <vector>
#include <iostream>
#include <cmath>
#include <Eigen/Dense>

typedef struct _Vertex_ {
	double v1;
	double v2;
	double v3;
	int sx;
	int sy;
	bool inside;
	_Vertex_(): v1(0), v2(0), v3(0), sx(0), sy(0), inside(true) {}
	_Vertex_(double v1, double v2, double v3): v1(v1), v2(v2), v3(v3), sx(0), sy(0), inside(true) {}
	~_Vertex_() {}
} Vertex;

typedef struct _Face_{
	int f1;
	int f2;
	int f3;
	_Face_(): f1(0), f2(0), f3(0) {}
	_Face_(int f1, int f2, int f3): f1(f1), f2(f2), f3(f3) {}
	~_Face_() {}
} Face;

class ObjModel{
	friend class Renderer;

	std::string name;
	std::vector<Vertex> m_vertices;
	std::vector<Face> m_faces;
	std::vector<Eigen::MatrixXd> m_matrices;
	Eigen::MatrixXd transmatrix;


public:
	ObjModel();
	~ObjModel();
	ObjModel(const ObjModel* source);

	void setName(const std::string s);
	std::string getName(void);

	void addVertex(double v1, double v2, double v3);
	Vertex getVertex(int ind);

	void addFace(int f1, int f2, int f3);
	Face getFace(int ind);
	
	void setTranslation(double x, double y, double z);
	void setRotation(double x, double y, double z, double theta);
	void setScale(double x, double y, double z);
	void computeTransMat(void);
	void transformVerticesToWorld(void);
	void transformVerticesToNDC(Eigen::MatrixXd mat);
	void transformVerticesToScene(int xres, int yres);

	void printObjModelInfo(void);

};

inline const char* const BoolToStr(bool b){
	return b ? "true" : "false";
}



#endif