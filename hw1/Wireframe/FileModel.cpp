#include "FileModel.h"

using namespace std;

FileModel::FileModel(){

}

FileModel::~FileModel(){

}

int FileModel::parse(char * filename){
	if(filename){	
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return -1;
		}

		vector<string> elems;
		string str;

		getline(inf, str);
		if(str != "camera:"){
			return -1;
		}

		// get postion
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "position" || elems.size() != 4){
			return -1;
		}
		m_cpos = CPosition(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));

		// get m_cori
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "orientation" || elems.size() != 5){
			return -1;
		}
		m_cori = COrientation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL), stod(elems[4], NULL));

		// get perspective parameter
		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "near" || elems.size() != 2){
			return -1;
		}
		m_ppara.near = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "far" || elems.size() != 2){
			return -1;
		}
		m_ppara.far = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "left" || elems.size() != 2){
			return -1;
		}
		m_ppara.left = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "right" || elems.size() != 2){
			return -1;
		}
		m_ppara.right = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "top" || elems.size() != 2){
			return -1;
		}
		m_ppara.top = stod(elems[1], NULL);

		getline(inf, str);
		elems = split(str, ' ');
		if(elems[0] != "bottom" || elems.size() != 2){
			return -1;
		}
		m_ppara.bottom = stod(elems[1], NULL);

		getline(inf, str);
		getline(inf, str);
		if(str != "objects:"){
			return -1;
		}

		vector<ObjModel> initialmodels;
		ObjModel* initialmodel;
		ObjModel transformedmodel;

		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			int size = elems.size();
			if(size == 2){
				parseObjFile(initialmodels, elems[0], elems[1]);
			}
			if(size == 1){
				if(!(initialmodel = search(initialmodels, elems[0]))){
					return -1;
				}
				transformedmodel = ObjModel(initialmodel);
			}
			if(size == 4 || size == 5){
				if(elems[0] == "t"){
					transformedmodel.setTranslation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
				if(elems[0] == "r"){
					transformedmodel.setRotation(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL), stod(elems[4], NULL));
				}
				if(elems[0] == "s"){
					transformedmodel.setScale(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				}
			}
			if(size == 0 && transformedmodel.getName() != ""){
				m_transformedmodels.push_back(transformedmodel);
				transformedmodel.setName("");
			}
		}
		return 0;
	}
	return -1;
}

int FileModel::parseObjFile(vector<ObjModel> &initialmodels, const string modelname, const string &filename){
	if(!filename.empty()){
		ifstream inf(filename);
		if(!inf){
			cerr << "Can't open file %s.\n" << filename << endl;
			return -1;
		}

		ObjModel model;
		model.setName(modelname);

		vector<string> elems;
		string str;
		while(inf){
			getline(inf, str);
			elems = split(str, ' ');
			if(elems.size() == 4){
				if(elems[0] == "v"){
					model.addVertex(stod(elems[1], NULL), stod(elems[2], NULL), stod(elems[3], NULL));
				} else{
					model.addFace(stoi(elems[1], NULL), stoi(elems[2], NULL), stoi(elems[3], NULL));
				}
			}
		}
		initialmodels.push_back(model);
	}

	return 0;
}

ObjModel* FileModel::search(vector<ObjModel> &initialmodels, const string &s){
	int size = initialmodels.size();
	for(int i = 0; i < size; i++){
		if(s.compare(initialmodels[i].getName()) == 0){
			return &(initialmodels[i]);
		}
	}
	return NULL;
}

vector<string> FileModel::split(const string &s, char delim){
	vector<string> elems;
	stringstream ss;
	string item;

	ss.str(s);
	while(getline(ss, item, delim)){
		elems.push_back(item);
	}

	return elems;
}
