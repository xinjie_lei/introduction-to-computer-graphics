#ifndef RENDERER_H
#define RENDERER_H

#include <Eigen/Dense>
#include <iostream>
#include <string>
#include <fstream>
#include "FileModel.h"

class Renderer{
	int xres;
	int yres;
	FileModel m_model;
	Eigen::MatrixXd m_wtcMat;
	Eigen::MatrixXd m_ctwMat;
	Eigen::MatrixXd m_ppMat;
	Eigen::MatrixXd m_wtNDCMat;
	std::vector<std::vector<char>> m_image;

public:
	Renderer();
	~Renderer();

	void setImageSize(char* xres, char* yres);
	int parse(char * filename);
	void setWorldToCameraMatrix(void);
	void setCameraToWorldMatrix(void);
	void setPerspectiveMatrix(void);
	void setTransfromToNDCMatrix(void);
	void transformGeometry(void);
	void transformWorldToNDC(void);
	void transformNDCtoScene(int xres, int yres);
	void generateImage(void);
	void line(int x0, int y0, int x1, int y1);
	int generatePPM(void);
	void printVerticesinNDC(void);
};

#endif